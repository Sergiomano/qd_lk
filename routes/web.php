<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', "UI\HomeController@home");

Route::post('/AddUsers', "UI\UserController@AddUsers");

Route::post('/CheckLogin', "UI\UserController@CheckLogin");

Route::get('/logout', "UI\UserController@logout");

Route::get('/Users/Profile', "UI\UserController@profile");

Route::get('/Users/Dashboard', "UI\UserController@dashboard");

Route::get('/Users/PostAds', "UI\UserController@post_ads");

Route::get('/Listings', "UI\PostController@listings");

Route::get('/Ads/MyAds', "UI\PostController@my_ads");

Route::get('/Listing_details/AdsDetails/{categories}/{id}', "UI\PostController@listing_details");

Route::get('/Listing_details/PaidAdsDetails/{categories}/{id}', "UI\PostController@paid_listing_details");

Route::post('/AddPosts', "UI\PostController@AddPosts");

Route::get('/EditPosts/{id}', "UI\UserController@edit_ads");

Route::post('/UpdatePosts', "UI\PostController@UpdatePosts");

Route::post('/ProfileAddOrUpdate', "UI\UserController@ProfileAddOrUpdate");

Route::post('/GetSubcategories/{id}', "UI\PostController@GetSubcategories");

Route::post('/AddToFavourites', "UI\PostController@AddToFavourites");

Route::get('/SearchAds', "UI\PostController@SearchAds");

Route::get('/MyFavourites', "UI\UserController@my_favourites");



// Route::get('image/upload','PostController@fileCreate');
Route::post('image/upload/store','UI\PostController@fileStore');
Route::post('image/delete','UI\PostController@fileDestroy');


Route::post('image/upload/updatestore','UI\PostController@EditfileStore');

Route::get('/Categories/{id}', "UI\PostController@GetAdsByCategories");

Route::get('searchajax',array('as'=>'searchajax','uses'=>'UI\PostController@SearchAdsName'));

Route::post('/GetSortBy', "UI\PostController@GetSortByAds");













// Admin Routes
Route::get('/Admin/Login', "Admin\AuthendicationController@Login");

Route::get('/Admin/Dashboard', "Admin\AuthendicationController@dashboard");

Route::get('/Admin/Cities', "Admin\AttributesController@Cities");

Route::post('/AddCities', "Admin\AttributesController@AddCities");

Route::post('/GetCities/{id}', "Admin\AttributesController@GetCities");

Route::post('/UpdateCities', "Admin\AttributesController@UpdateCities");

Route::post('/DeleteCities/{id}', "Admin\AttributesController@DeleteCities");

Route::get('/HomepageAds', "Admin\AttributesController@HomepageAds");

Route::post('/AddHomeTopAds', "Admin\AttributesController@AddHomeTopAds");

Route::post('/AddHomeFeaturedTopAds', "Admin\AttributesController@AddHomeFeaturedTopAds");

Route::post('/AddHomeBottomTopAds', "Admin\AttributesController@AddHomeBottomTopAds");

Route::post('/AddHomeLeftAds', "Admin\AttributesController@AddHomeLeftAds");

Route::post('/AddHomeRightAds', "Admin\AttributesController@AddHomeRightAds");


Route::get('/HomepageLeftAds', "Admin\AttributesController@HomepageLeftAds");

Route::get('/HomepageRightAds', "Admin\AttributesController@HomepageRightAds");


Route::get('/Admin/AddAds', "Admin\AdsController@AddAds");


Route::post('/Admin/GetSubcategories/{id}', "Admin\AttributesController@GetSubcategories");


Route::post('/Admin/AddPosts', "Admin\AdsController@AddPosts");

Route::post('image/upload/Adminstore','Admin\AdsController@fileStore');

Route::get('/Admin/AdsList', "Admin\AdsController@AdsList");


Route::get('/Admin/EditPosts/{id}', "Admin\AdsController@EditAds");


Route::post('/DeletePictures/{id}', "UI\UserController@DeletePictures");
