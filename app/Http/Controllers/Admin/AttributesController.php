<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\CitiesModel;
use App\Model\Admin\AdsModel;
use App\Model\Admin\HomeLeftAdsModel;
use App\Model\Admin\HomeRightAdsModel;
use App\Model\UI\CategoriesModel;

class AttributesController extends Controller
{
    public function Cities(){
        $title = "Cities list";

        $GetCities = CitiesModel::get();
        return view("Admin.layouts.cities", compact('title', 'GetCities'));
    }

    public function AddCities(Request $request){
        $City = new CitiesModel();

        $City->name = $request->city;
        $InsertCity = $City->save();

        if($InsertCity){
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"City added successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
        ));
        }

    }




    public function HomepageAds(){
        $title = "Home Ads";

        $GetCities = CitiesModel::get();

        $GetAds = AdsModel::get();

        return view("Admin.layouts.home_ads", compact('title', 'GetCities', 'GetAds'));
    }


    public function HomepageLeftAds(){
        $title = "Home Left Ads";

        $GetCities = CitiesModel::get();

        $GetAds = HomeLeftAdsModel::get();

        return view("Admin.layouts.home_left_ads", compact('title', 'GetCities', 'GetAds'));
    }


    public function HomepageRightAds(){
        $title = "Home Right Ads";

        $GetCities = CitiesModel::get();

        $GetAds = HomeRightAdsModel::get();

        return view("Admin.layouts.home_right_ads", compact('title', 'GetCities', 'GetAds'));
    }


    public function AddHomeTopAds(Request $request){
        $GetId = $request->id;

        $GetAds = AdsModel::find($GetId);

        if($GetId){
            $Ads = AdsModel::find($GetId);

            $Ads->home_top_ads_link = $request->ads_link;



            if($request->file('file') == null){
                $GetAds->home_top_ads = $Ads->home_top_ads;


            $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'AdsImage/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
                // $Ads->home_top_ads = $filename;
                $Ads->home_top_ads = $filename;

            $AddPosts = $Ads->save();
            }


            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Updated Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }else{
            $Ads = new AdsModel;

            $Ads->home_top_ads_link = $request->ads_link;
            $Ads->home_top_ads = $filename;

            $AddPosts = $Ads->save();
            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Added Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }

    }



    public function AddHomeFeaturedTopAds(Request $request){
        $GetId = $request->id;

        $GetAds = AdsModel::find($GetId);


        if($GetId){
            $Ads = AdsModel::find($GetId);

            $Ads->home_top_featured_ads_link = $request->featured_ads_link;



            if($request->file('file') == null){
                $GetAds->home_top_featured_ads = $Ads->home_top_featured_ads;


            $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'AdsImage/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
                // $Ads->home_top_ads = $filename;
                $Ads->home_top_featured_ads = $filename;

            $AddPosts = $Ads->save();
            }


            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Updated Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }else{
            $Ads = new AdsModel;

            $Ads->home_top_featured_ads_link = $request->featured_ads_link;
            $Ads->home_top_featured_ads = $filename;

            $AddPosts = $Ads->save();
            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Added Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }

    }



    public function AddHomeBottomTopAds(Request $request){
        $GetId = $request->id;

        $GetAds = AdsModel::find($GetId);


        if($GetId){
            $Ads = AdsModel::find($GetId);

            $Ads->home_top_bottom_ads_link = $request->bottom_ads_link;



            if($request->file('file') == null){
                $GetAds->home_top_bottom_ads = $Ads->home_top_bottom_ads;


            $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'AdsImage/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
                // $Ads->home_top_ads = $filename;
                $Ads->home_top_bottom_ads = $filename;

            $AddPosts = $Ads->save();
            }


            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Updated Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }else{
            $Ads = new AdsModel;

            $Ads->home_top_bottom_ads_link = $request->bottom_ads_link;
            $Ads->home_top_bottom_ads = $filename;

            $AddPosts = $Ads->save();
            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Added Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }

    }


    public function GetCities($Id){
        $GetCities = CitiesModel::find($Id);
        echo json_encode($GetCities);
    }

    public function UpdateCities(Request $request){
        $Id = $request->id;

        $City = CitiesModel::find($Id);

        $City->name = $request->city;
        $InsertCity = $City->save();

        if($InsertCity){
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"City updated successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
        ));
        }

    }

    public function DeleteCities($Id){
        $DeleteCity = CitiesModel::where('id', $Id)->delete();


        if($DeleteCity){
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"City deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
        ));
        }
    }







    // Ads
    public function AddHomeLeftAds(Request $request){
        $GetId = $request->id;

        $GetAds = HomeLeftAdsModel::find($GetId);

        if($GetAds){
            // echo json_encode($GetAds);
            // exit;
            $Ads = HomeLeftAdsModel::find($GetId);

            $Ads->ads_link1 = $request->ads_link1;

            $Ads->ads_link2 = $request->ads_link2;

            $Ads->ads_link3 = $request->ads_link3;

            $Ads->ads_link4 = $request->ads_link4;

            $Ads->ads_link5 = $request->ads_link5;

            $Ads->ads_link6 = $request->ads_link6;

            $Ads->ads_link7 = $request->ads_link7;

            $Ads->ads_link8 = $request->ads_link8;

            $Ads->ads_link9 = $request->ads_link9;

            $Ads->ads_link10 = $request->ads_link10;



            if($request->file('file1') == null){
                $GetAds->ads_1 = $Ads->ads_1;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file1')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file1')->move($dir, $filename1);
                $Ads->ads_1 = $filename1;

                $AddPosts = $Ads->save();
            }


            if($request->file('file2') == null){
                $GetAds->ads_2 = $Ads->ads_2;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file2')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename2 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file2')->move($dir, $filename1);
                $Ads->ads_2 = $filename2;

                $AddPosts = $Ads->save();
            }

            if($request->file('file3') == null){
                $GetAds->ads_3 = $Ads->ads_3;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file3')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename3 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file3')->move($dir, $filename1);
                $Ads->ads_3 = $filename3;

                $AddPosts = $Ads->save();
            }

            if($request->file('file4') == null){
                $GetAds->ads_4 = $Ads->ads_4;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file4')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename4 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file4')->move($dir, $filename4);
                $Ads->ads_4 = $filename4;

                $AddPosts = $Ads->save();
            }

            if($request->file('file5') == null){
                $GetAds->ads_5 = $Ads->ads_5;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file5')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename5 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file5')->move($dir, $filename5);
                $Ads->ads_5 = $filename5;

                $AddPosts = $Ads->save();
            }

            if($request->file('file6') == null){
                $GetAds->ads_6 = $Ads->ads_6;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file6')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename6 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file6')->move($dir, $filename6);
                $Ads->ads_6 = $filename6;

                $AddPosts = $Ads->save();
            }

            if($request->file('file7') == null){
                $GetAds->ads_7 = $Ads->ads_7;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file7')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename7 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file7')->move($dir, $filename7);
                $Ads->ads_7 = $filename7;

                $AddPosts = $Ads->save();
            }

            if($request->file('file8') == null){
                $GetAds->ads_8 = $Ads->ads_8;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file8')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename8 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file8')->move($dir, $filename8);
                $Ads->ads_8 = $filename8;

                $AddPosts = $Ads->save();
            }

            if($request->file('file9') == null){
                $GetAds->ads_9 = $Ads->ads_9;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file9')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename9 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file9')->move($dir, $filename9);
                $Ads->ads_9 = $filename9;

                $AddPosts = $Ads->save();
            }

            if($request->file('file10') == null){
                $GetAds->ads_10 = $Ads->ads_10;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file10')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename10 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file10')->move($dir, $filename10);
                $Ads->ads_10 = $filename10;

                $AddPosts = $Ads->save();
            }


            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Updated Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }else{
            $Ads = new HomeLeftAdsModel;

            $extension = $request->file('file1')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename1 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file1')->move($dir, $filename1);

        $extension = $request->file('file2')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename2 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file2')->move($dir, $filename2);

        $extension = $request->file('file3')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename3 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file3')->move($dir, $filename3);

        $extension = $request->file('file4')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename4 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file4')->move($dir, $filename4);

        $extension = $request->file('file5')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename5 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file5')->move($dir, $filename5);

        $extension = $request->file('file6')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename6 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file6')->move($dir, $filename6);

        $extension = $request->file('file7')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename7 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file7')->move($dir, $filename7);

        $extension = $request->file('file8')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename8 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file8')->move($dir, $filename8);

        $extension = $request->file('file9')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename9 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file9')->move($dir, $filename9);

        $extension = $request->file('file10')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename10 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file10')->move($dir, $filename10);

            $Ads->ads_link1 = $request->ads_link1;
            $Ads->ads_1 = $filename1;

            $Ads->ads_link2 = $request->ads_link2;
            $Ads->ads_2 = $filename2;

            $Ads->ads_link3 = $request->ads_link3;
            $Ads->ads_3 = $filename3;

            $Ads->ads_link4 = $request->ads_link4;
            $Ads->ads_4 = $filename4;

            $Ads->ads_link5 = $request->ads_link5;
            $Ads->ads_5 = $filename5;

            $Ads->ads_link6 = $request->ads_link6;
            $Ads->ads_6 = $filename6;

            $Ads->ads_link7 = $request->ads_link7;
            $Ads->ads_7 = $filename7;

            $Ads->ads_link8 = $request->ads_link8;
            $Ads->ads_8 = $filename8;

            $Ads->ads_link9 = $request->ads_link9;
            $Ads->ads_9 = $filename9;

            $Ads->ads_link10 = $request->ads_link10;
            $Ads->ads_10 = $filename10;

            $AddPosts = $Ads->save();
            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Added Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }

    }




    public function AddHomeRightAds(Request $request){
        $GetId = $request->id;

        $GetAds = HomeRightAdsModel::find($GetId);

        if($GetAds){
            // echo json_encode($GetAds);
            // exit;
            $Ads = HomeRightAdsModel::find($GetId);

            $Ads->ads_link1 = $request->ads_link1;

            $Ads->ads_link2 = $request->ads_link2;

            $Ads->ads_link3 = $request->ads_link3;

            $Ads->ads_link4 = $request->ads_link4;

            $Ads->ads_link5 = $request->ads_link5;

            $Ads->ads_link6 = $request->ads_link6;

            $Ads->ads_link7 = $request->ads_link7;

            $Ads->ads_link8 = $request->ads_link8;

            $Ads->ads_link9 = $request->ads_link9;

            $Ads->ads_link10 = $request->ads_link10;



            if($request->file('file1') == null){
                $GetAds->ads_1 = $Ads->ads_1;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file1')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file1')->move($dir, $filename1);
                $Ads->ads_1 = $filename1;

                $AddPosts = $Ads->save();
            }


            if($request->file('file2') == null){
                $GetAds->ads_2 = $Ads->ads_2;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file2')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename2 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file2')->move($dir, $filename1);
                $Ads->ads_2 = $filename2;

                $AddPosts = $Ads->save();
            }

            if($request->file('file3') == null){
                $GetAds->ads_3 = $Ads->ads_3;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file3')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename3 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file3')->move($dir, $filename1);
                $Ads->ads_3 = $filename3;

                $AddPosts = $Ads->save();
            }

            if($request->file('file4') == null){
                $GetAds->ads_4 = $Ads->ads_4;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file4')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename4 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file4')->move($dir, $filename4);
                $Ads->ads_4 = $filename4;

                $AddPosts = $Ads->save();
            }

            if($request->file('file5') == null){
                $GetAds->ads_5 = $Ads->ads_5;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file5')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename5 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file5')->move($dir, $filename5);
                $Ads->ads_5 = $filename5;

                $AddPosts = $Ads->save();
            }

            if($request->file('file6') == null){
                $GetAds->ads_6 = $Ads->ads_6;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file6')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename6 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file6')->move($dir, $filename6);
                $Ads->ads_6 = $filename6;

                $AddPosts = $Ads->save();
            }

            if($request->file('file7') == null){
                $GetAds->ads_7 = $Ads->ads_7;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file7')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename7 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file7')->move($dir, $filename7);
                $Ads->ads_7 = $filename7;

                $AddPosts = $Ads->save();
            }

            if($request->file('file8') == null){
                $GetAds->ads_8 = $Ads->ads_8;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file8')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename8 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file8')->move($dir, $filename8);
                $Ads->ads_8 = $filename8;

                $AddPosts = $Ads->save();
            }

            if($request->file('file9') == null){
                $GetAds->ads_9 = $Ads->ads_9;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file9')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename9 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file9')->move($dir, $filename9);
                $Ads->ads_9 = $filename9;

                $AddPosts = $Ads->save();
            }

            if($request->file('file10') == null){
                $GetAds->ads_10 = $Ads->ads_10;
                $AddPosts = $Ads->save();

            }else{
                $extension = $request->file('file10')->getClientOriginalExtension();
                $dir = 'HomeLeftImages/';
                $filename10 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('file10')->move($dir, $filename10);
                $Ads->ads_10 = $filename10;

                $AddPosts = $Ads->save();
            }


            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Updated Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }else{
            $Ads = new HomeRightAdsModel;

            $extension = $request->file('file1')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename1 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file1')->move($dir, $filename1);

        $extension = $request->file('file2')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename2 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file2')->move($dir, $filename2);

        $extension = $request->file('file3')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename3 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file3')->move($dir, $filename3);

        $extension = $request->file('file4')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename4 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file4')->move($dir, $filename4);

        $extension = $request->file('file5')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename5 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file5')->move($dir, $filename5);

        $extension = $request->file('file6')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename6 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file6')->move($dir, $filename6);

        $extension = $request->file('file7')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename7 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file7')->move($dir, $filename7);

        $extension = $request->file('file8')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename8 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file8')->move($dir, $filename8);

        $extension = $request->file('file9')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename9 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file9')->move($dir, $filename9);

        $extension = $request->file('file10')->getClientOriginalExtension();
        $dir = 'HomeLeftImages/';
        $filename10 = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file10')->move($dir, $filename10);

            $Ads->ads_link1 = $request->ads_link1;
            $Ads->ads_1 = $filename1;

            $Ads->ads_link2 = $request->ads_link2;
            $Ads->ads_2 = $filename2;

            $Ads->ads_link3 = $request->ads_link3;
            $Ads->ads_3 = $filename3;

            $Ads->ads_link4 = $request->ads_link4;
            $Ads->ads_4 = $filename4;

            $Ads->ads_link5 = $request->ads_link5;
            $Ads->ads_5 = $filename5;

            $Ads->ads_link6 = $request->ads_link6;
            $Ads->ads_6 = $filename6;

            $Ads->ads_link7 = $request->ads_link7;
            $Ads->ads_7 = $filename7;

            $Ads->ads_link8 = $request->ads_link8;
            $Ads->ads_8 = $filename8;

            $Ads->ads_link9 = $request->ads_link9;
            $Ads->ads_9 = $filename9;

            $Ads->ads_link10 = $request->ads_link10;
            $Ads->ads_10 = $filename10;

            $AddPosts = $Ads->save();
            // $GetId = $Posts->id;

            if($AddPosts){

                return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Ads Added Successfully"
                ));
            }else{
                return response()->json(array(
                    "error"=>TRUE,
                    "message"=>"Failed"
                ));
            }
        }

    }


    public function GetSubcategories($Id){
        $GetSubCategories = CategoriesModel::where('parent_id', $Id)->get();
        // echo json_encode($GetSubCategories);
        // $GetSubCategoriesHtml = "";
        foreach($GetSubCategories as $SubCategories){
            $GetSubCategoriesHtml = "<li id=".$SubCategories->id."><span>".$SubCategories->name."</span></li>";
            echo $GetSubCategoriesHtml;
        }

    }

}
