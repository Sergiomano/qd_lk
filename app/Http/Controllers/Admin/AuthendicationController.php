<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthendicationController extends Controller
{
    public function Login(){
        $title = "Sign in";
        // $GetCategories = CategoriesModel::where('parent_id', '0')->get();

        // $GetCities = CitiesModel::get();
        return view("Admin.layouts.signin", compact('title'));
    }


    public function dashboard(){
        $title = "Dashboard";
        // $GetCategories = CategoriesModel::where('parent_id', '0')->get();

        // $GetCities = CitiesModel::get();
        return view("Admin.layouts.dashboard", compact('title'));
    }
}
