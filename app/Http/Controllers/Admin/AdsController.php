<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\AdsModel;
use App\Model\UI\CitiesModel;
use App\Model\UI\CategoriesModel;
use App\Model\UI\PostAdsModel;
use App\Model\UI\PicturesModel;
use App\Model\UI\Post_pictures;
use App\Model\UI\SubCategoriesModel;

use DB;
use Session;

class AdsController extends Controller
{

    public function AdsList(){
        $title = "Ads List";

        // $GetAds = PostAdsModel::where('post_type', 1)->get();
        $GetAds = DB::table('posts')
                    ->select('posts.*', 'categories.name', 'pictures.filename')
                    ->join('categories', 'categories.id', '=', 'posts.category_id')
                    ->join('pictures', 'posts.id', '=', 'pictures.post_id')
                    ->where('posts.post_type', 1)
                    ->get();

        // $GetCities = CitiesModel::get();
        // $GetCategories = CategoriesModel::where('parent_id', '0')->get();
        return view("Admin.layouts.ads_list", compact('title', 'GetAds'));
    }

    public function AddAds(){
        $title = "Add Ads";

        $GetAds = AdsModel::get();

        $GetCities = CitiesModel::get();
        $GetCategories = CategoriesModel::where('parent_id', '0')->get();
        return view("Admin.layouts.ads", compact('title', 'GetAds', 'GetCities', 'GetCategories'));
    }


    public function EditAds($GetId){
        $title = "Edit Ads";


        $GetCities = CitiesModel::get();
        $GetCategories = CategoriesModel::where('parent_id', '0')->get();

        $GetPosts = PostAdsModel::where('id', $GetId)->first();

        // echo json_encode($GetPosts);
        // exit;

        $GetFeaturedImage = PicturesModel::where('post_id', $GetId)->first();

        $GetSubcategories = SubCategoriesModel::get();

        $GetPostImages = Post_pictures::where('post_id', $GetId)->get();

        return view("Admin.layouts.edit_ads", compact('title', 'GetCities', 'GetCategories', 'GetFeaturedImage', 'GetPosts', 'GetSubcategories', 'GetPostImages'));
    }


    public function AddPosts(Request $request){
        $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'Pay_Featured_Image/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);

        // $GetUserId = Session::get('UsersId');
        // echo $GetUserId;
        // exit;
        $Posts = new PostAdsModel;
        $Posts->title = $request->title;
        // $Posts->user_id = $GetUserId;
        $Posts->ads_id = rand(10,100);
        $Posts->category_id = $request->categories;
        $Posts->sub_category_id = $request->subcategories;
        $Posts->description = $request->description;
        $Posts->price = $request->price;
        $Posts->city_id = $request->city;
        $Posts->tags = $request->tags;
        $Posts->post_type = "1";
        // $Posts->user_id = $request->email;
        // $Posts->featured_images = $filename;
        $Posts->ip_addr = $request->ip();

        $AddPosts = $Posts->save();
        $request->session()->put('PostId', $Posts->id);

        $FeaturedImage = new PicturesModel();
        $FeaturedImage->post_id = $Posts->id;
        $FeaturedImage->filename = $filename;
        $FeaturedImage->save();
        // $GetId = $Posts->id;

        if($AddPosts){

            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Posts Added Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function fileStore(Request $request)
    {
        $GetPostId = Session::get("PostId");

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('Pay_Images'),$imageName);

        $imageUpload = new Post_pictures();
        $imageUpload->post_images = $imageName;
        $imageUpload->post_id = $GetPostId;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }
}


