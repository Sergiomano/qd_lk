<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\PostAdsModel;
use App\Model\UI\CategoriesModel;
use App\Model\UI\CitiesModel;

use App\Model\Admin\AdsModel;
use App\Model\Admin\HomeLeftAdsModel;
use App\Model\Admin\HomeRightAdsModel;
use DB;

class HomeController extends Controller
{
    public function home(){
        $title = "Home";

        // $GetLatestAds = DB::table('posts')->Orderby('created_at', 'desc')->limit(6)->get();
        $GetCategories = CategoriesModel::where('parent_id', '0')->get();
        $GetCities = CitiesModel::get();

        $GetLatestAds =  DB::select('SELECT DISTINCT categories.id, categories.name AS CategoryName, users.name AS UserName, posts.title, posts.created_at, posts.price, cities.name As CityName, pictures.filename, posts.id AS AdsId  FROM posts, categories, users, cities, pictures WHERE posts.category_id = categories.id AND posts.user_id = users.id AND posts.city_id = cities.id AND posts.id = pictures.post_id ORDER BY posts.created_at DESC LIMIT 15');


        // $GetPostId = "";
        // foreach($GetLatestAds as $Ads){
        //     $GetPostId = $Ads->AdsId;
        // }

        // $CHeckFavourites = DB::select("SELECT * FROM favourites, posts WHERE favourites.post_id = $GetPostId AND favourites.status = 1 ORDER BY posts.created_at DESC LIMIT 6");

        // echo json_encode($GetLatestAds);
        // exit;

        $GetFeaturedAds =  DB::select('SELECT categories.name AS CategoryName, posts.title, posts.created_at, posts.price, cities.name As CityName  FROM posts, categories, cities WHERE posts.category_id = categories.id  AND  posts.featured = 1 AND posts.city_id = cities.id ORDER BY posts.created_at DESC LIMIT 6');

        $GetAds = AdsModel::get();

        $GetLeftAds = HomeLeftAdsModel::get();

        $GetRightAds = HomeRightAdsModel::get();

        $GetHomeCategories = CategoriesModel::where('parent_id', '0')->limit(12)->get();

        return view("UI.layouts.home", compact('title', 'GetLatestAds', 'GetFeaturedAds', 'GetCategories', 'GetCities', 'GetAds', 'GetHomeCategories', 'GetLeftAds', 'GetRightAds'));
    }
}
