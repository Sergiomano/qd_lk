<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UI\UserModel;
use App\Model\UI\PostAdsModel;
use App\Model\UI\PicturesModel;
use App\Model\UI\Post_pictures;
use App\Model\UI\CategoriesModel;
use App\Model\UI\CitiesModel;
use App\Model\UI\FavouritesModel;
use App\Model\Admin\AdsModel;
use Session;
use DB;
use DateTime;

class PostController extends Controller
{
    public function listings(){
        $title = "Listings";
        $GetCategories = CategoriesModel::where('parent_id', '0')->get();
        // $GetPosts = PostAdsModel::take(10)->get();
        // $GetPosts = DB::select("SELECT posts.title, posts.id,posts.category_id, categories.name, posts.price, cities.name AS CityName, posts.address, posts.created_at, users.name AS UserName, posts.featured_images FROM posts, categories, cities, users WHERE posts.category_id = categories.id  AND posts.user_id = users.id AND posts.city_id = cities.id ORDER BY posts.created_at DESC LIMIT 10");

        $GetPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'users.name AS UserName', 'posts.featured_images')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->orderby('posts.created_at', 'desc')
                    ->paginate(10);

        return view("UI.layouts.listings", compact('title', 'GetPosts', 'GetCategories'));
    }

    public function listing_details(){
        $title = "Listings Details";
        $GetCategoryId = request()->segment(3);
        $GetId = request()->segment(4);
        // $GetPosts = PostAdsModel::take(10)->get();
        $GetPosts = DB::select("SELECT posts.title, posts.description,posts.email, posts.category_id, posts.phone, categories.name, posts.price, cities.name AS CityName, posts.address, posts.created_at, users.name AS UserName, posts.id AS AdsId FROM posts, categories, cities, users WHERE posts.category_id = categories.id  AND posts.user_id = users.id AND posts.city_id = cities.id AND posts.id = $GetId");

        $GetPostImages = DB::select("SELECT * FROM post_images WHERE post_images.post_id = $GetId");

        $GetPostId = "";
        foreach($GetPosts as $Ads){
            $GetPostId = $Ads->AdsId;
        }

        $CHeckFavourites = DB::select("SELECT * FROM favourites WHERE favourites.post_id = $GetId AND favourites.status = 1");

        // echo json_encode($GetPostImages);
        // exit;
        $GetCities = CitiesModel::get();

        $GetFeaturedProducts = DB::select("SELECT * FROM posts WHERE category_id = $GetCategoryId  LIMIT 3");
        return view("UI.layouts.ads_details", compact('title', 'GetPosts', 'GetFeaturedProducts', 'GetPostImages', 'GetCities', 'CHeckFavourites'));
    }

    public function paid_listing_details(){
        $title = "Listings Details";
        $GetCategoryId = request()->segment(3);
        $GetId = request()->segment(4);
        // $GetPosts = PostAdsModel::take(10)->get();
        $GetPosts = DB::select("SELECT DISTINCT posts.title, posts.description,posts.email, posts.category_id, posts.phone, categories.name, posts.price, cities.name AS CityName, posts.address, posts.created_at, posts.id AS AdsId FROM posts, categories, cities WHERE posts.category_id = categories.id  AND posts.city_id = cities.id AND posts.id = $GetId");

        $GetPostImages = DB::select("SELECT * FROM post_images WHERE post_images.post_id = $GetId");

        $GetPostId = "";
        foreach($GetPosts as $Ads){
            $GetPostId = $Ads->AdsId;
        }

        $CHeckFavourites = DB::select("SELECT * FROM favourites WHERE favourites.post_id = $GetId AND favourites.status = 1");

        // echo json_encode($GetPosts);
        // exit;
        $GetCities = CitiesModel::get();

        $GetFeaturedProducts = DB::select("SELECT * FROM posts WHERE category_id = $GetCategoryId  LIMIT 3");
        return view("UI.layouts.paid_ads_details", compact('title', 'GetPosts', 'GetFeaturedProducts', 'GetPostImages', 'GetCities', 'CHeckFavourites'));
    }

    public function my_ads(){
        $title = "My Ads";
        $GetUserId = Session::get('UsersId');

        // $GetUserId = 51;
        // $GetMyAds = PostAdsModel::where('user_id',   $GetUserId)
        //             ->get();

        $GetMyAds = DB::table('posts')
                    ->select('posts.title','posts.ads_id', 'categories.name', 'posts.price', 'posts.category_id', 'posts.id', 'categories.name', 'pictures.filename')
                    ->where('posts.user_id', $GetUserId)
                    ->join('categories', 'posts.category_id', '=', 'categories.id')
                    ->join('pictures', 'pictures.post_id', '=', 'posts.id')
                    ->get();

        // echo json_encode($GetMyAds);
        // exit;
        $GetFeaturedMyAds = DB::table('posts')
                    ->select('posts.*', 'categories.name', 'pictures.filename')
                    ->where('posts.featured', 1)
                    ->where('posts.user_id', $GetUserId)
                    ->join('categories', 'posts.category_id', '=', 'categories.id')
                    ->join('pictures', 'pictures.post_id', '=', 'posts.id')
                    ->get();

        $GetActiveMyAds = DB::table('posts')
                    ->select('posts.*', 'categories.name', 'pictures.filename')
                    ->where('posts.status', 0)
                    ->where('posts.user_id', $GetUserId)
                    ->join('categories', 'posts.category_id', '=', 'categories.id')
                    ->join('pictures', 'pictures.post_id', '=', 'posts.id')
                    ->get();

        $GetExpiryMyAds = DB::table('posts')
                    ->select('posts.*', 'categories.name', 'pictures.filename')
                    ->where('posts.status', 1)
                    ->where('posts.user_id', $GetUserId)
                    ->join('categories', 'posts.category_id', '=', 'categories.id')
                    ->join('pictures', 'pictures.post_id', '=', 'posts.id')
                    ->get();

        $GetCities = CitiesModel::get();
        return view("UI.layouts.my_ads", compact('title', 'GetMyAds', 'GetFeaturedMyAds', 'GetActiveMyAds', 'GetCities', 'GetExpiryMyAds'));
    }

    public function AddPosts(Request $request){
        $extension = $request->file('file')->getClientOriginalExtension();
        $dir = 'Post_Featured_image/';
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);

        $GetUserId = Session::get('UsersId');
        // echo $GetUserId;
        // exit;
        $Posts = new PostAdsModel;
        $Posts->title = $request->title;
        $Posts->user_id = $GetUserId;
        $Posts->ads_id = rand(10,100);
        $Posts->category_id = $request->categories;
        $Posts->sub_category_id = $request->subcategories;
        $Posts->description = $request->description;
        $Posts->price = $request->price;
        $Posts->city_id = $request->city;
        $Posts->tags = $request->tags;
        // $Posts->user_id = $request->email;
        // $Posts->featured_images = $filename;
        $Posts->ip_addr = $request->ip();

        $AddPosts = $Posts->save();
        $request->session()->put('PostId', $Posts->id);

        $FeaturedImage = new PicturesModel();
        $FeaturedImage->post_id = $Posts->id;
        $FeaturedImage->filename = $filename;
        $FeaturedImage->save();
        // $GetId = $Posts->id;

        if($AddPosts){

            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Posts Added Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function UpdatePosts(Request $request){
        // $extension = $request->file('file')->getClientOriginalExtension();
        // $dir = 'Post_Featured_image/';
        // $filename = uniqid() . '_' . time() . '.' . $extension;
        // $request->file('file')->move($dir, $filename);

        $GetUserId = Session::get('UsersId');
        // echo $request->subcategories;;
        // exit;
        $AdsId = $request->AdsId;
        $Posts = PostAdsModel::findOrFail($AdsId);
        $Posts->title = $request->title;
        $Posts->user_id = $GetUserId;
        $Posts->ads_id = rand(10,100);
        $Posts->category_id = $request->categories;
        $Posts->sub_category_id = $request->subcategories;
        $Posts->description = $request->description;
        $Posts->price = $request->price;
        $Posts->city_id = $request->city;
        $Posts->tags = $request->tags;
        // $Posts->user_id = $request->email;
        // $Posts->featured_images = $filename;
        $Posts->ip_addr = $request->ip();

        $AddPosts = $Posts->save();
        $request->session()->put('PostId', $Posts->id);

        // $FeaturedImage = new PicturesModel();
        // $FeaturedImage->post_id = $Posts->id;
        // $FeaturedImage->filename = $filename;
        // $FeaturedImage->save();
        // $GetId = $Posts->id;

        if($AddPosts){

            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Posts Updated Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    public function fileStore(Request $request)
    {
        $GetPostId = Session::get("PostId");

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('Post_images'),$imageName);

        $imageUpload = new Post_pictures();
        $imageUpload->post_images = $imageName;
        $imageUpload->post_id = $GetPostId;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }


    public function EditfileStore(Request $request)
    {
        $GetPostId = $request->AdsId;

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('Post_images'),$imageName);

        $imageUpload = new Post_pictures();
        $imageUpload->post_images = $imageName;
        $imageUpload->post_id = $GetPostId;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }

    public function fileDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        PicturesModel::where('filename',$filename)->delete();
        $path=public_path().'/Post_images'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }

    public function GetSubcategories($Id){
        $GetSubCategories = CategoriesModel::where('parent_id', $Id)->get();
        // echo json_encode($GetSubCategories);
        // $GetSubCategoriesHtml = "";
        foreach($GetSubCategories as $SubCategories){
            $GetSubCategoriesHtml = "<option value=".$SubCategories->id.">".$SubCategories->name."</option>";
            echo $GetSubCategoriesHtml;
        }

    }



    public function SearchAds(Request $request){
        $title = "Search Results";
        $GetAdsName = $request->ads_name;
        $GetAdsLocation = $request->location;

        $GetCities = CitiesModel::get();

        $GetCategories = CategoriesModel::where('parent_id', '0')->get();
        $GetAds = AdsModel::get();
        // $GetPosts = PostAdsModel::take(10)->get();
        $GetPosts = DB::select("SELECT posts.title, posts.id,posts.category_id, categories.name, posts.price, cities.name AS CityName, posts.address, posts.created_at, posts.featured_images, users.name AS UserName, posts.post_type FROM posts, categories, cities, users WHERE posts.category_id = categories.id  AND posts.user_id = users.id AND posts.city_id = cities.id AND posts.title LIKE '%$GetAdsName%' AND posts.post_type = 0 AND cities.id = '$GetAdsLocation' ORDER BY posts.created_at DESC LIMIT 10");
        return view("UI.layouts.listings", compact('title', 'GetPosts', 'GetCategories', 'GetCities', 'GetAds'));
    }

    public function AddToFavourites(Request $request){
        $Favourites = new FavouritesModel();

        $UserId = Session::get('UsersId');
        $PostId = $request->post_id;
        // $CheckFavouritesStatus = $request->FavouritesStatus;
        // echo json_encode($UserId);
        // exit;

        $CheckFavourites = FavouritesModel::where('user_id', $UserId)
                            ->where('post_id', $PostId)
                            ->first();

        if($CheckFavourites != null){
            if($CheckFavourites["status"] == 0){
                $CheckFavourites->status = 1;
                $CheckFavourites->save();
            }else{
                $CheckFavourites->status = 0;
                $CheckFavourites->save();
            }
        }else{
            $AddFavourite = new FavouritesModel();

            $AddFavourite->user_id = $UserId;
            $AddFavourite->post_id = $PostId;
            $AddFavourite->status = 1;
            $AddFavourite->save();

        }

    }


    public function GetAdsByCategories(Request $request){
        $title = "Get Ads";

        $GetId = request()->segment(2);

        $GetAdsName = $request->ads_name;
        $GetAdsLocation = $request->location;

        $GetCities = CitiesModel::get();

        $GetCategories = CategoriesModel::where('parent_id', '0')->get();
        $GetAds = AdsModel::get();
        // $GetPosts = PostAdsModel::take(10)->get();
        // $GetPosts = DB::select("SELECT posts.title, posts.id,posts.category_id, categories.name, posts.price, cities.name AS CityName, posts.address, posts.created_at, users.name AS UserName, posts.featured_images FROM posts, categories, cities, users WHERE posts.category_id = categories.id  AND posts.user_id = users.id AND posts.city_id = cities.id AND posts.category_id = '$GetId' ORDER BY posts.created_at DESC LIMIT 10");

        $GetPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'users.name AS UserName', 'posts.featured_images', 'posts.post_type')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->where('posts.category_id', '=', $GetId)
                    ->where('posts.post_type', '=', 0)
                    ->orderby('posts.created_at', 'desc')
                    ->paginate(10);

        $GetPaymentPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'posts.featured_images', 'posts.post_type', 'pictures.filename')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    // ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->join('pictures', 'posts.id', '=', 'pictures.post_id')
                    ->where('posts.category_id', '=', $GetId)
                    ->where('posts.post_type', '=', 1)
                    ->orderby('posts.created_at', 'desc')
                    ->limit(2)
                    ->inRandomOrder()
                    ->get();

        // echo json_encode($GetPaymentPosts);
        // exit;

        return view("UI.layouts.listings", compact('title', 'GetPosts', 'GetCategories', 'GetCities', 'GetAds', 'GetPaymentPosts'));
    }


    public function SearchAdsName(Request $request){
        // $title = $request->term;
        // // echo $title;
        // // exit;

        // $GetTrainings = DB::select("SELECT qualification FROM qualifications WHERE qualification LIKE '%$title%' ORDER BY qualification ASC");

        // echo json_encode($GetTrainings);
        $query = $request->get('term','');

        $products=PostAdsModel::where('title','LIKE',$query.'%')
                                ->select('title')
                                ->distinct()
                                ->orderBy('title', 'ASC')
                                ->get();

        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->title,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }


    public function GetSortByAds(Request $request){
        $GetSortBy = $request->sortby;

        if($GetSortBy == 1){
            $GetPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'users.name AS UserName', 'posts.featured_images')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->orderby('posts.price', 'desc')
                    ->paginate(10);
        }elseif($GetSortBy == 2){
            $GetPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'users.name AS UserName', 'posts.featured_images')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->orderby('posts.price', 'asc')
                    ->paginate(10);
        }elseif($GetSortBy == 3){
            $GetPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'users.name AS UserName', 'posts.featured_images')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->orderby('posts.created_at', 'desc')
                    ->paginate(10);

        }elseif($GetSortBy == 4){
            $GetPosts = DB::table('posts')
                    ->select('posts.title', 'posts.id', 'posts.category_id', 'categories.name', 'posts.price', 'cities.name AS CityName', 'posts.address', 'posts.created_at', 'users.name AS UserName', 'posts.featured_images')
                    ->join('categories', 'categories.id' ,'=', 'posts.category_id')
                    ->join('cities', 'cities.id' ,'=', 'posts.city_id')
                    ->join('users', 'users.id' ,'=', 'posts.user_id')
                    ->orderby('posts.created_at', 'asc')
                    ->paginate(10);
        }


        foreach($GetPosts as $Posts){

                $GetPostedOn =
                $now = new DateTime;
                $full = false;
                $ago = new DateTime($Posts->created_at);
                $diff = $now->diff($ago);

                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;

                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }

                if (!$full) $string = array_slice($string, 0, 1);

                $GetPostedOn =  $string ? implode(', ', $string) . ' ago' : 'just now';


                $GetAds = "<div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                            <div class='featured-box'>
                            <figure>
                                <a href='#'>
                                    <img class='img-fluid' src='/Post_Featured_image/".$Posts->featured_images."' alt=''>
                                </a>
                            </figure>
                            <div class='feature-content'>
                                <div class='product'>
                                <a href='#'>
                                    <i class='lni-folder'>
                                    </i> ".$Posts->name."
                                </a>
                                </div>
                                <h4>
                                <a href='/Listing_details/AdsDetails/".$Posts->category_id."/".$Posts->id."'>".$Posts->title."
                                </a>
                                </h4>
                                <span>Last Updated: ".$GetPostedOn."
                                </span>
                                <ul class='address'>
                                <li>
                                    <a href='#'>
                                    <i class='lni-map-marker'>
                                    </i>".$Posts->address.", ".$Posts->CityName."
                                    </a>
                                </li>
                                <li>
                                    <a href='#'>
                                    <i class='lni-alarm-clock'>
                                    </i> ".date('M d Y', strtotime($Posts->created_at))."
                                    </a>
                                </li>
                                <li>
                                    <a href='#'>
                                    <i class='lni-user'>
                                    </i> ".$Posts->UserName."
                                    </a>
                                </li>
                                </ul>
                                <div class='listing-bottom'>
                                <h3 class='price float-left'>$".$Posts->price."
                                </h3>
                                </div>
                            </div>
                            </div>
                        </div>";

            if($GetAds){
                echo $GetAds;
            }else{
                echo "No Results Found...";
            }

        }



    }
}
