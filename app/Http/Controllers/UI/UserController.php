<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use App\Model\UI\UserModel;
use App\Model\UI\CategoriesModel;
use App\Model\UI\CitiesModel;
use App\Model\UI\PostAdsModel;
use App\Model\UI\SubCategoriesModel;
use App\Model\UI\PicturesModel;
use App\Model\Admin\AdsModel;

use App\Model\UI\Post_pictures;

use Session;
use DB;

class UserController extends Controller
{
    public function logout(Request $request) {
        // Auth::logout();
        Auth::guard('qduser')->logout();
        return redirect('/');
    }

    public function profile(){
        $title = "Home";
        $GetId = Session::get("UsersId");
        $GetUsers = UserModel::find($GetId);
        $GetCities = CitiesModel::get();

        return view("UI.layouts.user_profile", compact('title', 'GetUsers', 'GetCities'));
    }

    public function dashboard(){
        $title = "Dashboard";
        $GetCities = CitiesModel::get();
        $GetUserId = Session::get('UsersId');

        // $GetUserId = 51;

        $GetFeaturedAds =  DB::select("SELECT categories.name AS CategoryName, posts.title, posts.created_at, posts.price, cities.name As CityName  FROM posts, categories, cities WHERE posts.category_id = categories.id  AND  posts.featured = 1 AND posts.city_id = cities.id AND posts.user_id = $GetUserId");

        $GetAds =  DB::table('posts')
                    ->select('posts.title','posts.ads_id', 'categories.name', 'posts.price', 'posts.category_id', 'posts.id', 'posts.status', 'categories.name', 'pictures.filename')
                    ->where('posts.user_id', $GetUserId)
                    ->join('categories', 'posts.category_id', '=', 'categories.id')
                    ->join('pictures', 'pictures.post_id', '=', 'posts.id')
                    ->get();

        $GetHomeAdsAds = AdsModel::get();
        return view("UI.layouts.dashboard", compact('title', 'GetCities', 'GetAds', 'GetFeaturedAds', 'GetHomeAdsAds'));
    }

    public function post_ads(){
        $title = "Post Ads";
        $GetCategories = CategoriesModel::where('parent_id', '0')->get();

        $GetCities = CitiesModel::get();
        return view("UI.layouts.post_ads", compact('title', 'GetCategories', 'GetCities'));
    }

    public function edit_ads($GetId){
        $title = "Edit Ads";
        $GetCategories = CategoriesModel::where('parent_id', '0')->get();

        $GetCities = CitiesModel::get();

        $GetPosts = PostAdsModel::find($GetId);

        $GetFeaturedImage = PicturesModel::where('post_id', $GetId)->first();

        $GetSubcategories = SubCategoriesModel::get();

        $GetPostImages = Post_pictures::where('post_id', $GetId)->get();
        // echo json_encode($GetPostImages);
        // exit;
        return view("UI.layouts.edit_posts", compact('title', 'GetCategories', 'GetCities', 'GetPosts', 'GetSubcategories', 'GetFeaturedImage', 'GetPostImages'));
    }

    public function CheckLogin(Request $request){

        // echo $request->login_name;
        // echo $request->login_password;
        // exit;

        if (Auth::guard('qduser')->attempt(['username' => $request->login_name, 'password' => $request->login_password])) {
            //Authentication passed...
            $request->session()->put('UsersName', Auth::guard('qduser')->user()->username);
            $request->session()->put('UsersEmail', Auth::guard('qduser')->user()->email);
            $request->session()->put('UsersId', Auth::guard('qduser')->user()->id);
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"Login successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Please Check your credentials..."
            ));
        }
    }


    public function AddUsers(Request $request){
        $Users = new UserModel;
        $Users->username = $request->name;
        $Users->email = $request->email;
        $Users->password = Hash::make($request->password);
        $Users->ip_addr = $request->ip();

        $CheckEmail = UserModel::where('email', $request->email)->first();

        if($CheckEmail){
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Email is already used."
            ));
        }else{
            $AddUsers = $Users->save();
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"User registered successfully."
            ));
        }
    }


    public function ProfileAddOrUpdate(Request $request){
        $GetUserId = Session::get("UsersId");
        $Users = UserModel::where('id', $GetUserId)->first();

        $Users->name = $request->name;
        $Users->phone = $request->phone;
        $Users->email = $request->email;
        $Users->address = $request->address;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $AddUsers = $Users->save();
        if($AddUsers){
            return response()->json(array(
                "error"=>FALSE,
                "message"=>"User Profile Added Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    public function my_favourites(){
        $title = "My Favourites";
        $GetUserId = Session::get('UsersId');

        // $GetUserId = 51;
        // $GetMyAds = PostAdsModel::where('user_id', $GetUserId)
        //             ->get();
        $GetCities = CitiesModel::get();


        $GetMyFavAds = DB::table('posts')
                    ->select('posts.title', 'favourites.status' ,'posts.ads_id', 'categories.name', 'posts.price', 'posts.category_id', 'posts.id', 'pictures.filename')
                    ->where('favourites.user_id', $GetUserId)
                    ->join('categories', 'posts.category_id', '=', 'categories.id')
                    ->join('pictures', 'pictures.post_id', '=', 'posts.id')
                    ->join('favourites', 'favourites.post_id', '=', 'posts.id')
                    ->where('favourites.status', 1)
                    ->get();

        // echo json_encode($GetMyFavAds);
        // exit;

        return view("UI.layouts.my_favourites", compact('title', 'GetMyFavAds', 'GetCities'));
    }


    public function DeletePictures($Id){
        $DeletePictures = Post_pictures::where('id', $Id)->delete();


        if($DeletePictures){
            return response()->json(array(
                    "error"=>FALSE,
                    "message"=>"Post pictures deleted successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
        ));
        }
    }
}
