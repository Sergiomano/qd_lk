<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class AdsModel extends Model
{
    protected $table = "ads";

    protected $fillable = ['id', '	home_top_ads'];
}
