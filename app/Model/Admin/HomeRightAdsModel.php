<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class HomeRightAdsModel extends Model
{
    protected $table = "home_right_ads";

    protected $fillable = ['id', 'ads_link1', 'ads_1', 'ads_link2', 'ads_2', 'ads_link3', 'ads_3', 'ads_link4', 'ads_4', 'ads_link5', 'ads_5', 'ads_link6', 'ads_6', 'ads_link7', 'ads_7', 'ads_link8', 'ads_8', 'ads_link9', 'ads_9', 'ads_link10', 'ads_10' ];
}
