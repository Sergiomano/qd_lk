<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable
{
    protected $guard = 'qduser';

    protected $table = "users";
    
    protected $fillable = ['id', 'country_code', 'language_code', 'user_type_id', 'gender_id', 'name', 'about', 'phone', 'phone_hidden', 'username', 'email', 'password', 'remember_token', 'ip_addr', 'blocked', 'disable_comments', 'email_token', '	verified_email', 'verified_phone'];
}
