<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class FavouritesModel extends Model
{
    protected $table = "favourites";

    protected $fillable = ['id', 'user_id', 'post_id'];
}
