<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class CategoriesModel extends Model
{
    protected $table = "categories";
    
    protected $fillable = ['id', 'name', 'slug', 'picture'];
}
