<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class Post_pictures extends Model
{
    protected $table = "post_images";
    
    protected $fillable = ['id', 'post_id', 'post_images'];
}
