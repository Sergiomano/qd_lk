<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class SubCategoriesModel extends Model
{
    protected $table = "categories";
    
    protected $fillable = ['id', 'name', 'slug', 'picture'];
}
