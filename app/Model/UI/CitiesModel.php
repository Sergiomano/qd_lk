<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class CitiesModel extends Model
{
    protected $table = "cities";
    
    protected $fillable = ['id', 'country_code', 'name'];
}
