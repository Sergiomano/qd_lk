<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class PicturesModel extends Model
{
    protected $table = "pictures";
    
    protected $fillable = ['id', 'post_id', 'filename', 'active'];
}
