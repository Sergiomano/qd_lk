<?php

namespace App\Model\UI;

use Illuminate\Database\Eloquent\Model;

class PostAdsModel extends Model
{
    protected $table = "posts";
    
    protected $fillable = ['id', 'title', 'user_id', 'category_id', 'description', 'price', 'city_id', 'featured_images', 'tags', 'ip_addr', 'reviewed', 'featured'];
}
