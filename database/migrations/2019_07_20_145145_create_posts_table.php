<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('country_code', 2)->nullable()->index('country_code');
			$table->integer('user_id')->unsigned()->nullable()->index('user_id');
			$table->integer('category_id')->unsigned()->default(0)->index('category_id');
			$table->integer('post_type_id')->unsigned()->nullable()->index('post_type_id');
			$table->string('title')->default('')->index('title');
			$table->text('description', 65535);
			$table->string('tags')->nullable()->index('tags');
			$table->decimal('price', 17)->unsigned()->nullable();
			$table->boolean('negotiable')->nullable()->default(0);
			$table->string('contact_name', 200)->nullable()->index('contact_name');
			$table->string('email', 100)->nullable();
			$table->string('phone', 50)->nullable();
			$table->boolean('phone_hidden')->nullable()->default(0);
			$table->string('address')->nullable()->index('address');
			$table->integer('city_id')->unsigned()->default(0)->index('city_id');
			$table->float('lon', 10, 0)->nullable()->comment('longitude in decimal degrees (wgs84)');
			$table->float('lat', 10, 0)->nullable()->comment('latitude in decimal degrees (wgs84)');
			$table->string('ip_addr', 50)->nullable();
			$table->integer('visits')->unsigned()->nullable()->default(0);
			$table->string('email_token', 32)->nullable();
			$table->string('phone_token', 32)->nullable();
			$table->string('tmp_token', 32)->nullable();
			$table->boolean('verified_email')->nullable()->default(0)->index('verified_email');
			$table->boolean('verified_phone')->nullable()->default(1)->index('verified_phone');
			$table->boolean('reviewed')->nullable()->default(0)->index('reviewed');
			$table->boolean('featured')->nullable()->default(0)->index('featured');
			$table->boolean('archived')->nullable()->default(0);
			$table->string('fb_profile')->nullable();
			$table->string('partner', 50)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->index(['lon','lat'], 'lat');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
