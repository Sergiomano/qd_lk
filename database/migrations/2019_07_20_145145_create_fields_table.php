<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->enum('belongs_to', array('posts','users'))->index('belongs_to');
			$table->string('translation_lang', 10)->nullable()->index('translation_lang');
			$table->integer('translation_of')->unsigned()->nullable()->index('translation_of');
			$table->string('name', 100)->nullable();
			$table->enum('type', array('text','textarea','checkbox','checkbox_multiple','select','radio','file'))->default('text');
			$table->integer('max')->unsigned()->nullable()->default(255);
			$table->string('default')->nullable();
			$table->boolean('required')->nullable();
			$table->string('help')->nullable();
			$table->boolean('active')->nullable()->index('active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fields');
	}

}
