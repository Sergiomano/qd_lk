<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title}}
    </title>
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/bootstrap.min.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/fonts/line-icons.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/slicknav.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/color-switcher.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/nivo-lightbox.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/animate.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/owl.carousel.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/main.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/responsive.css")}}">

    <link rel="stylesheet" type="text/css" href="{{asset("UI/css/summernote.css")}}">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
{{-- DropZne --}}

    <link rel="stylesheet" href="https://www.dropzonejs.com/css/dropzone.css?v=1533562669">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css" rel="stylesheet">

    {{-- Notify --}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css">
  </head>
  <body>
    @include('UI.common.header')

    @yield('Content')

    @include('UI.common.footer')

    <div id="LoginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
              <h5 class="modal-title">Sign in</h5>
            </div>
            <div class="modal-body">
                    <div class="login-form login-area" id="LoginForm">
                            <form role="form" class="login-form">
                            <div class="form-group">
                            <div class="input-icon">
                            <i class="lni-user"></i>
                            <input type="text" id="login_name" class="form-control" name="email" placeholder="Username">
                            </div>
                            </div>
                            <div class="form-group">
                            <div class="input-icon">
                            <i class="lni-lock"></i>
                            <input type="password" id="user_password" class="form-control" placeholder="Password">
                            </div>
                            </div>
                            <div class="form-group mb-3">
                            <div class="checkbox">
                            <input type="checkbox" name="rememberme" value="rememberme">
                            <label>Keep me logged in</label>
                            </div>
                            <a class="forgetpassword" href="#">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <img src="{{asset("UI/ajax_loader.gif")}}" class="login-loader-center" id="AjaxLoader" style="display:none;" alt="">
                          </div>
                            <div class="text-center">
                            <button type="button" id="SignIn" class="btn btn-common log-btn">Sign In</button>
                              <p>Or</p>
                              <button type="button" id="Register" class="btn btn-common log-btn">Sign Up</button>
                          </div>
                            </form>
                        </div>

                        <div class="login-form login-area" id="RegisterForm" style="display:none;">
                          <form role="form" class="login-form">
                          <div class="form-group">
                          <div class="input-icon">
                          <i class="lni-user"></i>
                          <input type="text" id="reg_name" class="form-control" name="" placeholder="Username">
                          </div>
                          </div>
                          <div class="form-group">
                          <div class="input-icon">
                          <i class="lni-user"></i>
                          <input type="text" id="reg_email" class="form-control" name="" placeholder="Email">
                          </div>
                          </div>
                          <div class="form-group">
                          <div class="input-icon">
                          <i class="lni-lock"></i>
                          <input type="password" id="reg_password" class="form-control" placeholder="Password">
                          </div>
                          </div>
                          <div class="form-group">
                          <div class="input-icon">
                          <i class="lni-lock"></i>
                          <input type="password" id="reg_confirm_password" class="form-control" placeholder="Confirm Password">
                          </div>
                          </div>

                          <div class="text-center">
                            <img src="{{asset("UI/ajax_loader.gif")}}" class="login-loader-center" id="AjaxRegisterLoader" style="display:none;" alt="">
                      </div>

                          <div class="text-center">
                          <button class="btn btn-common log-btn" type="button" id="Signup">Submit</button>
                            <p>Or</p>
                          <button class="btn btn-common log-btn" type="button" id="BacktoLogin">Sign In</button>
                        </div>
                          </form>
                      </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>


      {{-- Signup Modal --}}
      <div id="SignupModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                  <h5 class="modal-title">Sign up</h5>
                </div>
                <div class="modal-body">
                        <div class="login-form login-area">
                                <form role="form" class="login-form">
                                <div class="form-group">
                                <div class="input-icon">
                                <i class="lni-user"></i>
                                <input type="text" id="reg_name" class="form-control" name="" placeholder="Username">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="input-icon">
                                <i class="lni-user"></i>
                                <input type="text" id="reg_email" class="form-control" name="" placeholder="Email">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="input-icon">
                                <i class="lni-lock"></i>
                                <input type="password" id="reg_password" class="form-control" placeholder="Password">
                                </div>
                                </div>
                                <div class="form-group">
                                <div class="input-icon">
                                <i class="lni-lock"></i>
                                <input type="password" id="reg_confirm_password" class="form-control" placeholder="Confirm Password">
                                </div>
                                </div>
                                <div class="text-center">
                                <button class="btn btn-common log-btn" type="button" id="Signup">Submit</button>
                                </div>
                                </form>
                            </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>

            </div>
          </div>


    <a href="#" class="back-to-top">
      <i class="lni-chevron-up">
      </i>
    </a>
    <div id="preloader">
      <div class="loader" id="loader-1">
      </div>



    </div>
    <script src="{{asset("UI/js/jquery-min.js")}}">
    </script>
    <script src="{{asset("UI/js/popper.min.js")}}">
    </script>
    <script src="{{asset("UI/js/bootstrap.min.js")}}">
    </script>
    {{-- <script src="{{asset("UI/js/color-switcher.js")}}">
    </script> --}}
    <script src="{{asset("UI/js/jquery.counterup.min.js")}}">
    </script>
    <script src="{{asset("UI/js/waypoints.min.js")}}">
    </script>
    <script src="{{asset("UI/js/wow.js")}}">
    </script>
    <script src="{{asset("UI/js/owl.carousel.min.js")}}">
    </script>
    <script src="{{asset("UI/js/nivo-lightbox.js")}}">
    </script>
    <script src="{{asset("UI/js/jquery.slicknav.js")}}">
    </script>
    <script src="{{asset("UI/js/main.js")}}">
    </script>
    <script src="{{asset("UI/js/form-validator.min.js")}}">
    </script>
    <script src="{{asset("UI/js/contact-form-script.min.js")}}">
    </script>
    <script src="{{asset("UI/js/summernote.js")}}">
    </script>

<script src="{{asset("UI/js/summernote.js")}}"></script>

<script src="https://www.cssscript.com/demo/super-simple-javascript-message-toaster-toast-js/toast.js"></script>

{{-- Notify --}}
<script src="http://bootstrap-notify.remabledesigns.com/js/bootstrap-notify.min.js"></script>

    {{-- Custom JS --}}
    <script src="{{asset("UI/js/custom/credentials.js")}}"></script>
    <script src="{{asset("UI/js/custom/users.js")}}"></script>
    <script src="{{asset("UI/js/custom/posts.js")}}"></script>

    {{-- CK Editor --}}
    <script src="https://cdn.ckeditor.com/4.9.2/basic/ckeditor.js"></script>

    <script>
			CKEDITOR.replace( 'post_description' );
    </script>


    {{-- Dropzone --}}
    <script src="https://www.dropzonejs.com/js/dropzone.js?v=1533562669"></script>


    {{-- Autocomplete JS--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>


    @yield('JSscript')

    {{--  <script>

    </script>  --}}
  </body>

</html>
