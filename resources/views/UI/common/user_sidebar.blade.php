<div class="user">
    <figure>
    <a href="#"><img src="{{asset("UI/img/author/img1.jpg")}}" alt=""></a>
    </figure>
    <div class="usercontent">
    <h3>User</h3>
    <h4>Administrator</h4>
    </div>
    </div>
    <nav class="navdashboard">
    <ul>
    <li>
    <a class="active" href="/Users/Dashboard">
    <i class="lni-dashboard"></i>
    <span>Dashboard</span>
    </a>
    </li>
    <li>
    <a href="/Users/Profile">
    <i class="lni-cog"></i>
    <span>Profile Settings</span>
    </a>
    </li>
    <li>
    <a href="/Users/PostAds">
    <i class="lni-layers"></i>
    <span>Post Ads</span>
    </a>
    </li>
    <li>
    <a href="/Ads/MyAds">
    <i class="lni-layers"></i>
    <span>My Ads</span>
    </a>
    </li>
    {{--  <li>
    <a href="offermessages.html">
    <i class="lni-envelope"></i>
    <span>Offers/Messages</span>
    </a>
    </li>  --}}
    {{-- <li>
    <a href="payments.html">
    <i class="lni-wallet"></i>
    <span>Payments</span>
    </a>
    </li> --}}
    <li>
    <a href="/MyFavourites">
    <i class="lni-heart"></i>
    <span>My Favourites</span>
    </a>
    </li>
    {{-- <li>
    <a href="privacy-setting.html">
    <i class="lni-star"></i>
    <span>Privacy Settings</span>
    </a>
     </li> --}}
    <li>
    <a href="#">
    <i class="lni-enter"></i>
    <span>Logout</span>
    </a>
    </li>
    </ul>
    </nav>
