<header id="header-wrap">
    <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar bg-white">
      <div class="container-fluid">
        <div class="row width100">
          <div class="col-md-2">
              <div class="navbar-header">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                    </span>
                    <span class="lni-menu">
                    </span>
                    <span class="lni-menu">
                    </span>
                    <span class="lni-menu">
                    </span>
                  </button>
                  <a href="/" class="navbar-brand">
                    {{-- <img src="{{URL::asset('UI\img\logo1.png')}}" id="normal_logo" alt=""> --}}
                    <img src="{{URL::asset('UI\img\logo.png')}}" id="scroller_logo" alt="">
                  </a>
                </div>
          </div>

          <div class="col-md-8">
              <div class="search-bar mar-top0">
                  <div class="search-inner bg-white">
                    <form class="search-form" action="/SearchAds">
                      <div class="form-group inputwithicon top-search-bar-width-location">
                          <i class="lni-map-marker">
                          </i>
                          <div class="select">
                            <select name="location">
                              <option value="none">Locations
                              </option>
                              @foreach($GetCities as $Cities)
                                <option value="{{$Cities->id}}">{{$Cities->name}}
                              </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      <div class="form-group inputwithicon top-search-bar-width-keyword">
                        <i class="lni-tag">
                        </i>
                        <input type="text" name="ads_name" id="ProductName" class="form-control" placeholder="Enter Product Keyword">
                      </div>

                      <button class="btn btn-common top-search-btn" type="submit">
                        <i class="lni-search">
                        </i>
                      </button>
                    </form>
                  </div>
                </div>
          </div>

          <div class="col-md-2 top-login-sell-btn">
              <div class="collapse navbar-collapse" id="main-navbar">
                  @if(Auth::guard('qduser')->check())
                  <ul class="sign-in">
                    <li class="nav-item dropdown">

                      <a class="nav-link dropdown-toggle my-account-width" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="lni-user">
                        </i> My Account
                      </a>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="/Users/Dashboard">
                          <i class="lni-home">
                          </i> Account Home
                        </a>
                        <a class="dropdown-item" href="/Users/Profile">
                          <i class="lni-wallet">
                          </i> Profile
                        </a>
                        <a class="dropdown-item" href="/Ads/MyAds">
                          <i class="lni-wallet">
                          </i> My Ads
                        </a>
                        <a class="dropdown-item" href="/MyFavourites">
                          <i class="lni-heart">
                          </i> Favourite ads
                        </a>
                        <a class="dropdown-item" href="/logout">
                          <i class="lni-close">
                          </i>Log out
                        </a>
                      </div>
                    </li>
                  </ul>
                  <a class="tg-btn top-btn-a sell-after-login" href="/Users/PostAds">
                    FREE SELL
                 </a>
                  @else
                  <a class="tg-btn top-btn-a top-login-link" href="#" data-toggle="modal" data-target="#LoginModal">
                     LOGIN
                  </a>

                  @if(Auth::guard('qduser')->check())
                  <a class="tg-btn top-btn-a" href="/Users/PostAds">
                    FREE SELL
                  </a>
                  @else
                  <a class="tg-btn top-btn-a" href="#" data-toggle="modal" data-target="#LoginModal">
                    FREE SELL
                 </a>
                  @endif

                  @endif
                </div>
          </div>
        </div>


      </div>
      <ul class="mobile-menu">
        <li>
          <a class="active" href="#">
            Home
          </a>
          <ul class="dropdown">
            <li>
              <a class="active" href="index-2.html">Home 1
              </a>
            </li>
            <li>
              <a href="index-3.html">Home 2
              </a>
            </li>
            <li>
              <a href="index-4.html">Home 2
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="category.html">Categories
          </a>
        </li>
        <li>
          <a href="#">
            Listings
          </a>
          <ul class="dropdown">
            <li>
              <a href="adlistinggrid.html">Ad Grid
              </a>
            </li>
            <li>
              <a href="adlistinglist.html">Ad Listing
              </a>
            </li>
            <li>
              <a href="ads-details.html">Listing Detail
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#">Pages
          </a>
          <ul class="dropdown">
            <li>
              <a href="about.html">About Us
              </a>
            </li>
            <li>
              <a href="services.html">Services
              </a>
            </li>
            <li>
              <a href="ads-details.html">Ads Details
              </a>
            </li>
            <li>
              <a href="post-ads.html">Ads Post
              </a>
            </li>
            <li>
              <a href="pricing.html">Packages
              </a>
            </li>
            <li>
              <a href="testimonial.html">Testimonial
              </a>
            </li>
            <li>
              <a href="faq.html">FAQ
              </a>
            </li>
            <li>
              <a href="404.html">404
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#">Blog
          </a>
          <ul class="dropdown">
            <li>
              <a href="blog.html">Blog - Right Sidebar
              </a>
            </li>
            <li>
              <a href="blog-left-sidebar.html">Blog - Left Sidebar
              </a>
            </li>
            <li>
              <a href="blog-grid-full-width.html"> Blog full width
              </a>
            </li>
            <li>
              <a href="single-post.html">Blog Details
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="contact.html">Contact Us
          </a>
        </li>
        <li>
          <a>My Account
          </a>
          <ul class="dropdown">
            <li>
              <a href="account-profile-setting.html">
                <i class="lni-home">
                </i> Account Home
              </a>
            </li>
            <li>
              <a href="account-myads.html">
                <i class="lni-wallet">
                </i> My Ads
              </a>
            </li>
            <li>
              <a href="account-favourite-ads.html">
                <i class="lni-heart">
                </i> Favourite ads
              </a>
            </li>
            <li>
              <a href="account-archived-ads.html">
                <i class="lni-folder">
                </i> Archived
              </a>
            </li>
            <li>
              <a href="login.html">
                <i class="lni-lock">
                </i> Log In
              </a>
            </li>
            <li>
              <a href="signup.html">
                <i class="lni-user">
                </i> Signup
              </a>
            </li>
            <li>
              <a href="forgot-password.html">
                <i class="lni-reload">
                </i> Forgot Password
              </a>
            </li>
            <li>
              <a href="account-close.html">
                <i class="lni-close">
                </i>Account close
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    @yield('HomeSearch')
  </header>


