@extends('UI.base')
@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-wrapper">
          <h2 class="product-title">Profile Settings
          </h2>
          <ol class="breadcrumb">
            <li>
              <a href="#">Home /
              </a>
            </li>
            <li class="current">Profile Settings
            </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="content" class="section-padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
        <aside>
          <div class="sidebar-box">
            @include('UI.common.user_sidebar')
          </div>
          <div class="widget">
            <h4 class="widget-title">Advertisement
            </h4>
            <div class="add-box">
              <img class="img-fluid" src="assets/img/img1.jpg" alt="">
            </div>
          </div>
        </aside>
      </div>
      <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="row page-content">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="inner-box">
              <div class="tg-contactdetail">
                <div class="dashboard-box">
                  <h2 class="dashbord-title">Contact Detail
                  </h2>
                </div>
                <div class="dashboard-wrapper">
                  <div class="form-group mb-3">
                    <label class="control-label">First Name*
                    </label>
                    @if($GetUsers)
                  <input class="form-control input-md" id="first_name" value="{{$GetUsers->name}}" type="text">
                    @else
                    <input class="form-control input-md" id="first_name" type="text">
                    @endif
                  </div>
                  <div class="form-group mb-3">
                    <label class="control-label">Last Name*
                    </label>
                    <input class="form-control input-md" id="last_name" type="text">
                  </div>
                  <div class="form-group mb-3">
                    <label class="control-label">Phone*
                    </label>
                    <input class="form-control input-md" id="phone" type="text">
                  </div>
                  <div class="form-group mb-3">
                    <label class="control-label">Email*
                    </label>
                    <input class="form-control input-md" id="email" type="text">
                  </div>
                  <div class="form-group mb-3">
                    <label class="control-label">Enter Address
                    </label>
                    <input class="form-control input-md" id="address" type="text">
                  </div>
                  {{-- <div class="form-group mb-3 tg-inputwithicon">
                    <label class="control-label">Country
                    </label>
                    <div class="tg-select form-control">
                      <select id="">
                        <option value="none">Select Country
                        </option>
                        <option value="none">New York
                        </option>
                        <option value="none">California
                        </option>
                        <option value="none">Washington
                        </option>
                        <option value="none">Birmingham
                        </option>
                        <option value="none">Chicago
                        </option>
                        <option value="none">Phoenix
                        </option>
                      </select>
                    </div>
                  </div> --}}
                  <div class="form-group mb-3 tg-inputwithicon">
                    <label class="control-label">State
                    </label>
                    <div class="tg-select form-control">
                      <select id="state">
                        <option value="none">Select State
                        </option>
                        <option value="none">Select State
                        </option>
                        <option value="none">Select State
                        </option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group mb-3 tg-inputwithicon">
                    <label class="control-label">City
                    </label>
                    <div class="tg-select form-control">
                      <select id="city">
                        <option value="none">Select State
                        </option>
                        <option value="none">Select State
                        </option>
                        <option value="none">Select State
                        </option>
                      </select>
                    </div>
                  </div>
                  {{--  <div class="tg-checkbox">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="tg-agreetermsandrules">
                      <label class="custom-control-label" for="tg-agreetermsandrules">I agree to all
                        <a href="javascript:void(0);">Terms of Use &amp; Posting Rules
                        </a>
                      </label>
                    </div>
                  </div>  --}}
                  <button class="btn btn-common" id="UpdateProfile" type="button">Update Profile
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
