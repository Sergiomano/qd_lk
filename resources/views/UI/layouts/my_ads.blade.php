@extends('UI.base')
@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="breadcrumb-wrapper">
              <h2 class="product-title">My Ads
              </h2>
              <ol class="breadcrumb">
                <li>
                  <a href="#">Home /
                  </a>
                </li>
                <li class="current">My Ads
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="content" class="section-padding">
      <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
                <aside>
                    <div class="sidebar-box">
                    @include('UI.common.user_sidebar')
                    </div>
                    <div class="widget">
                    <h4 class="widget-title">Advertisement
                    </h4>
                    <div class="add-box">
                        <img class="img-fluid" src="assets/img/img1.jpg" alt="">
                    </div>
                    </div>
                </aside>
                </div>
          <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="page-content">
              <div class="inner-box">
                <div class="dashboard-box">
                  <h2 class="dashbord-title">My Ads
                  </h2>
                </div>
                <div class="dashboard-wrapper">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-all-ads-tab" data-toggle="tab" href="#nav-all-ads" role="tab" aria-controls="nav-home" aria-selected="true">All Ads</a>
                            {{--  <a class="nav-item nav-link" id="nav-featured-tab" data-toggle="tab" href="#nav-featured" role="tab" aria-controls="nav-profile" aria-selected="false">Featured</a>  --}}
                            <a class="nav-item nav-link" id="nav-active-tab" data-toggle="tab" href="#nav-active" role="tab" aria-controls="nav-contact" aria-selected="false">Active</a>
                            <a class="nav-item nav-link" id="nav-expired-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Expired</a>
                        </div>
                    </nav>
                  <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-all-ads" role="tabpanel" aria-labelledby="nav-all-ads-tab">
                                <table class="table table-responsive dashboardtable tablemyads">
                                        <thead>
                                          <tr>
                                            <th>Photo
                                            </th>
                                            <th>Title
                                            </th>
                                            <th>Category
                                            </th>
                                            <th>Ad Status
                                            </th>
                                            <th>Price
                                            </th>
                                            <th>Action
                                            </th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetMyAds as $Ads)
                                          <tr data-category="active">
                                            <td class="photo">
                                              <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
                                            </td>
                                            <td data-title="Title">
                                              <h3>{{$Ads->title}}
                                              </h3>
                                              <span>Ad ID: {{$Ads->ads_id}}
                                              </span>
                                            </td>
                                            <td data-title="Category">
                                              <span class="adcategories">{{$Ads->name}}
                                              </span>
                                            </td>
                                            <td data-title="Ad Status">
                                              <span class="adstatus adstatusactive">active
                                              </span>
                                            </td>
                                            <td data-title="Price">
                                              <h3>{{$Ads->price}}
                                              </h3>
                                            </td>
                                            <td data-title="Action">
                                              <div class="btns-actions">
                                                <a class="btn-action btn-view" href="/Listing_details/AdsDetails/{{$Ads->category_id}}/{{$Ads->id}}" target="_blank">
                                                  <i class="lni-eye">
                                                  </i>
                                                </a>
                                                <a class="btn-action btn-edit" href="/EditPosts/{{$Ads->id}}">
                                                  <i class="lni-pencil">
                                                  </i>
                                                </a>
                                                {{--  <a class="btn-action btn-delete" href="#">
                                                  <i class="lni-trash">
                                                  </i>
                                                </a>  --}}
                                              </div>
                                            </td>
                                          </tr>
                                          @endforeach

                                        </tbody>
                                      </table>
                        </div>
                        <div class="tab-pane fade" id="nav-featured" role="tabpanel" aria-labelledby="nav-featured-tab">
                           <table class="table table-responsive dashboardtable tablemyads">
                                <thead>
                                <tr>
                                    <th>Photo
                                    </th>
                                    <th>Title
                                    </th>
                                    <th>Category
                                    </th>
                                    <th>Ad Status
                                    </th>
                                    <th>Price
                                    </th>
                                    <th>Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($GetFeaturedMyAds as $Ads)
                                <tr data-category="active">
                                    <td class="photo">
                                    <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
                                    </td>
                                    <td data-title="Title">
                                    <h3>{{$Ads->title}}
                                    </h3>
                                    <span>Ad ID: {{$Ads->ads_id}}
                                    </span>
                                    </td>
                                    <td data-title="Category">
                                    <span class="adcategories">{{$Ads->name}}
                                    </span>
                                    </td>
                                    <td data-title="Ad Status">
                                    <span class="adstatus adstatusactive">active
                                    </span>
                                    </td>
                                    <td data-title="Price">
                                    <h3>{{$Ads->price}}
                                    </h3>
                                    </td>
                                    <td data-title="Action">
                                    <div class="btns-actions">
                                        <a class="btn-action btn-view" href="/Listing_details/AdsDetails/{{$Ads->category_id}}/{{$Ads->id}}" target="_blank">
                                        <i class="lni-eye">
                                        </i>
                                        </a>
                                        <a class="btn-action btn-edit" href="#">
                                        <i class="lni-pencil">
                                        </i>
                                        </a>
                                        <a class="btn-action btn-delete" href="#">
                                        <i class="lni-trash">
                                        </i>
                                        </a>
                                    </div>
                                    </td>
                                </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="nav-active" role="tabpanel" aria-labelledby="nav-active-tab">
                                <table class="table table-responsive dashboardtable tablemyads">
                                        <thead>
                                        <tr>
                                            <th>Photo
                                            </th>
                                            <th>Title
                                            </th>
                                            <th>Category
                                            </th>
                                            <th>Ad Status
                                            </th>
                                            <th>Price
                                            </th>
                                            <th>Action
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($GetActiveMyAds as $Ads)
                                        <tr data-category="active">
                                            <td class="photo">
                                            <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
                                            </td>
                                            <td data-title="Title">
                                            <h3>{{$Ads->title}}
                                            </h3>
                                            <span>Ad ID: {{$Ads->ads_id}}
                                            </span>
                                            </td>
                                            <td data-title="Category">
                                            <span class="adcategories">{{$Ads->name}}
                                            </span>
                                            </td>
                                            <td data-title="Ad Status">
                                            <span class="adstatus adstatusactive">active
                                            </span>
                                            </td>
                                            <td data-title="Price">
                                            <h3>{{$Ads->price}}
                                            </h3>
                                            </td>
                                            <td data-title="Action">
                                            <div class="btns-actions">
                                                <a class="btn-action btn-view" href="/Listing_details/AdsDetails/{{$Ads->category_id}}/{{$Ads->id}}" target="_blank">
                                                <i class="lni-eye">
                                                </i>
                                                </a>
                                                <a class="btn-action btn-edit" href="#">
                                                <i class="lni-pencil">
                                                </i>
                                                </a>
                                                <a class="btn-action btn-delete" href="#">
                                                <i class="lni-trash">
                                                </i>
                                                </a>
                                            </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                        </div>
                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-expired-tab">
                            <table class="table" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Photo
                                        </th>
                                        <th>Title
                                        </th>
                                        <th>Category
                                        </th>
                                        <th>Ad Status
                                        </th>
                                        <th>Price
                                        </th>
                                        <th>Action
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($GetExpiryMyAds as $Ads)
                                    <tr data-category="active">
                                        <td class="photo">
                                        <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
                                        </td>
                                        <td data-title="Title">
                                        <h3>{{$Ads->title}}
                                        </h3>
                                        <span>Ad ID: {{$Ads->ads_id}}
                                        </span>
                                        </td>
                                        <td data-title="Category">
                                        <span class="adcategories">{{$Ads->name}}
                                        </span>
                                        </td>
                                        <td data-title="Ad Status">
                                        <span class="adstatus adstatusactive">active
                                        </span>
                                        </td>
                                        <td data-title="Price">
                                        <h3>{{$Ads->price}}
                                        </h3>
                                        </td>
                                        <td data-title="Action">
                                        <div class="btns-actions">
                                            <a class="btn-action btn-view" href="/Listing_details/AdsDetails/{{$Ads->category_id}}/{{$Ads->id}}" target="_blank">
                                            <i class="lni-eye">
                                            </i>
                                            </a>
                                            <a class="btn-action btn-edit" href="#">
                                            <i class="lni-pencil">
                                            </i>
                                            </a>
                                            <a class="btn-action btn-delete" href="#">
                                            <i class="lni-trash">
                                            </i>
                                            </a>
                                        </div>
                                        </td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                            </table>
                        </div>
                    </div>
                  {{-- <table class="table table-responsive dashboardtable tablemyads">
                    <thead>
                      <tr>
                        <th>
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="checkedall">
                            <label class="custom-control-label" for="checkedall">
                            </label>
                          </div>
                        </th>
                        <th>Photo
                        </th>
                        <th>Title
                        </th>
                        <th>Category
                        </th>
                        <th>Ad Status
                        </th>
                        <th>Price
                        </th>
                        <th>Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($GetMyAds as $Ads)
                      <tr data-category="active">
                        <td>
                          <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="adone">
                            <label class="custom-control-label" for="adone">
                            </label>
                          </div>
                        </td>
                        <td class="photo">
                          <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->featured_images")}}" alt="">
                        </td>
                        <td data-title="Title">
                          <h3>{{$Ads->title}}
                          </h3>
                          <span>Ad ID: {{$Ads->ads_id}}
                          </span>
                        </td>
                        <td data-title="Category">
                          <span class="adcategories">{{$Ads->name}}
                          </span>
                        </td>
                        <td data-title="Ad Status">
                          <span class="adstatus adstatusactive">active
                          </span>
                        </td>
                        <td data-title="Price">
                          <h3>{{$Ads->price}}
                          </h3>
                        </td>
                        <td data-title="Action">
                          <div class="btns-actions">
                            <a class="btn-action btn-view" href="/Listing_details/AdsDetails/{{$Ads->category_id}}/{{$Ads->id}}" target="_blank">
                              <i class="lni-eye">
                              </i>
                            </a>
                            <a class="btn-action btn-edit" href="#">
                              <i class="lni-pencil">
                              </i>
                            </a>
                            <a class="btn-action btn-delete" href="#">
                              <i class="lni-trash">
                              </i>
                            </a>
                          </div>
                        </td>
                      </tr>
                      @endforeach

                    </tbody>
                  </table> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection
