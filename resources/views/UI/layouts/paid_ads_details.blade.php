@extends('UI.base')
@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="breadcrumb-wrapper">
            <h2 class="product-title">Details
            </h2>
            <ol class="breadcrumb">
              <li>
                <a href="#">Home /
                </a>
              </li>
              <li class="current">Details
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  @foreach($GetPosts as $Posts)
  <div class="section-padding">
    <div class="container">
      <div class="product-info row">
        <div class="col-lg-7 col-md-12 col-xs-12">
          <div class="details-box ads-details-wrapper">
            <div id="owl-demo" class="owl-carousel owl-theme">
              @foreach($GetPostImages as $Images)
              <div class="item">
                <div class="product-img">
                <img class="img-fluid" src="{{asset("public/Pay_Images")}}/{{$Images->post_images}}" alt="">
                </div>
                <span class="price">{{$Posts->price}}
                </span>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="col-lg-5 col-md-12 col-xs-12">
          <div class="details-box">
            <div class="ads-details-info">
              <h2>{{$Posts->title}}
              </h2>
              <div class="details-meta">
                <span>
                  <a href="#">
                    <i class="lni-alarm-clock">
                    </i> {{date("M d Y", strtotime($Posts->created_at))}}
                  </a>
                </span>
                <span>
                  <a href="#">
                    <i class="lni-map-marker">
                    </i> {{$Posts->address}}
                  </a>
                </span>
                {{-- <span>
                  <a href="#">
                    <i class="lni-eye">
                    </i> 299 View
                  </a>
                </span> --}}
              </div>

              <p class="mb-2">{!!$Posts->description!!}
              </p>

            </div>
            <ul class="advertisement mb-4">
              <li>
                <p>
                  <strong>
                    <i class="lni-folder">
                    </i> Categories:
                  </strong>
                  <a href="#">{{$Posts->name}}
                  </a>
                </p>
              </li>
              {{-- <li>
                <p>
                  <strong>
                    <i class="lni-archive">
                    </i> Condition:
                  </strong> New
                </p>
              </li>
              <li>
                <p>
                  <strong>
                    <i class="lni-package">
                    </i> Brand:
                  </strong>
                  <a href="#">Apple
                  </a>
                </p>
              </li> --}}
            </ul>
            <div class="ads-btn mb-4">
                <div class="stage">
                  <div class="home-hear-bg-white"></div>
                  @if(Auth::guard('qduser')->check())
                  @if($CHeckFavourites)
                    <div class="heart pull-right heart-mar-right is-active" data-id = "{{$Posts->AdsId}}"></div>
                    @else
                    <div class="heart pull-right heart-mar-right" data-id = "{{$Posts->AdsId}}"></div>
                    @endif
                  </div>
                  @endif
              <a href="#" class="btn btn-common btn-reply">
                <i class="lni-envelope">
                </i> {{$Posts->email}}
              </a>
              <a href="#" class="btn btn-common">
                <i class="lni-phone-handset">
                </i> {{$Posts->phone}}
              </a>
            </div>
            <div class="share">
              <span>Share:
              </span>
              <div class="social-link">
                <a class="facebook" href="#">
                  <i class="lni-facebook-filled">
                  </i>
                </a>
                <a class="twitter" href="#">
                  <i class="lni-twitter-filled">
                  </i>
                </a>
                <a class="linkedin" href="#">
                  <i class="lni-linkedin-fill">
                  </i>
                </a>
                <a class="google" href="#">
                  <i class="lni-google-plus">
                  </i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="description-info">
        <div class="row">
          <div class="col-lg-8 col-md-6 col-xs-12">
            <div class="description">
              <h4>Description
              </h4>
              {!!$Posts->description!!}
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="short-info">
              {{--  <h4>Short Info
              </h4>  --}}
              <ul>
                {{--  <li>
                  <a href="#">
                    <i class="lni-users">
                    </i> More ads by
                    <span>User
                    </span>
                  </a>
                </li>  --}}
                {{-- <li>
                  <a href="#">
                    <i class="lni-printer">
                    </i> Print this ad
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="lni-reply">
                    </i> Send to a friend
                  </a>
                </li> --}}
                {{--  <li>
                  <a href="#">
                    <i class="lni-warning">
                    </i> Report this ad
                  </a>
                </li>  --}}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
  <section class="featured-lis section-padding">
    <div class="container">
      <h3 class="section-title">Featured Products
      </h3>
      <div class="row">
          @foreach($GetFeaturedProducts as $Products)
        <div class="col-lg-4 col-md-6 col-xs-12">
          <div class="product-item">
            <div class="carousel-thumb">
              <img class="img-fluid" src="{{asset("UI/img/product/img1.jpg")}}" alt="">
              <div class="overlay">
              </div>
              <div class="btn-product bg-yellow">
                <a href="#">New
                </a>
              </div>
            </div>
            <div class="product-content">
              <h3 class="product-title">
                <a href="/Listing_details/AdsDetails/{{$Products->category_id}}/{{$Products->id}}">{{$Products->title}}
                </a>
              </h3>
              <p class="ads_details-p">Lorem ipsum dolor sit
              </p>
              <br>
              <span class="price ads_details-price">{{$Products->price}}
              </span>
              {{-- <div class="meta">
                <span class="icon-wrap">
                  <i class="lni-star-filled">
                  </i>
                  <i class="lni-star-filled">
                  </i>
                  <i class="lni-star-filled">
                  </i>
                  <i class="lni-star">
                  </i>
                  <i class="lni-star">
                  </i>
                </span>
                <span class="count-review">
                  <span>1
                  </span> Reviews
                </span>
              </div> --}}
              <div class="card-text">
                <div class="float-left">
                  <a href="#">
                    <i class="lni-map-marker">
                    </i> {{$Products->address}}
                  </a>
                </div>
                {{-- <div class="float-right">
                  <div class="icon">
                    <i class="lni-heart">
                    </i>
                  </div>
                </div> --}}
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
@endsection
