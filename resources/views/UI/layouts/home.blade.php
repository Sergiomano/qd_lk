@extends('UI.base')

@section('HomeSearch')
<div id="hero-area">
        <div class="overlay">
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 text-center">
              <div class="contents">
                <div class="search-bar">
                  {{-- <div class="search-inner">
                    <form class="search-form" action="/SearchAds">
                      <div class="form-group inputwithicon">
                        <i class="lni-tag">
                        </i>
                        <input type="text" name="ads_name" class="form-control" placeholder="Enter Product Keyword">
                      </div>
                      <div class="form-group inputwithicon">
                        <i class="lni-map-marker">
                        </i>
                        <div class="select">
                          <select name="location">
                            <option value="none">Locations
                            </option>
                            @foreach($GetCities as $Cities)
                              <option value="{{$Cities->id}}">{{$Cities->name}}
                            </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group inputwithicon">
                        <i class="lni-menu">
                        </i>
                        <div class="select">
                          <select name="categories">
                            <option value="none">Categories
                            </option>
                            @foreach($GetCategories as $Categories)
                            <option value="{{$Categories->id}}">{{$Categories->name}}
                            </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <button class="btn btn-common" type="submit">
                        <i class="lni-search">
                        </i> Search Now
                      </button>
                    </form>
                  </div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('Content')
<section class="categories-icon section-padding bg-drack">
    <div class="container">
        <div class="row" id="TopAds">
            <div class="col-md-4">
              @foreach($GetAds as $Ads)
                  @if($Ads->home_top_ads)
                  <div class="text-center">
                      <a href="{{ $Ads->home_top_ads_link }}" target="_blank"> <img src="{{asset("AdsImage/$Ads->home_top_ads")}}" alt="" class="width100 ads-height mar-bottom15"></a>
                  </div>
                  @endif
              @endforeach
            </div>

            <div class="col-md-4">
              @foreach($GetAds as $Ads)
                  @if($Ads->home_top_ads)
                  <div class="text-center">
                      <a href="{{ $Ads->home_top_ads_link }}" target="_blank"> <img src="{{asset("AdsImage/$Ads->home_top_ads")}}" alt="" class="width100 ads-height mar-bottom15"></a>
                  </div>
                  @endif
              @endforeach
            </div>

            <div class="col-md-4">
              @foreach($GetAds as $Ads)
                  @if($Ads->home_top_ads)
                  <div class="text-center">
                      <a href="{{ $Ads->home_top_ads_link }}" target="_blank"> <img src="{{asset("AdsImage/$Ads->home_top_ads")}}" alt="" class="width100 ads-height mar-bottom15"></a>
                  </div>
                  @endif
              @endforeach
            </div>
        </div>
      <div class="row">
          @foreach($GetHomeCategories as $Categories)
        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
          <a href="/Categories/{{$Categories->id}}">
            <div class="icon-box">
              <div class="icon">
                <i class="lni-car">
                </i>
              </div>
              <h4>{{$Categories->name}}
              </h4>
            </div>
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <section class="featured section-padding">
    <div class="container-fluid">

      <h1 class="section-title section-title-latest-products">Latest Products
      </h1>
      <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 ads-mar-top" id="AdsLeft">
                @foreach($GetLeftAds as $Ads)
                    @if($Ads->ads_1)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link1 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_1")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif
                    <br>
                    @if($Ads->ads_2)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link2 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_2")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_3)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link3 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_3")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_4)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link4 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_4")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_5)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link5 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_5")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_6)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link6 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_6")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_7)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link7 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_7")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_8)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link8 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_8")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_9)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link9 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_9")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_10)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link10 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_10")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                @endforeach
            </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="row">
        @foreach($GetLatestAds as $Ads)
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
          <div class="featured-box">
            <figure>
              {{--  <div class="icon">
                  <i class="lni-heart">
                </i>

              </div>  --}}
              {{-- <div class="stage">
                  <div class="home-hear-bg-white"></div>
                  @if($Ads->status == 1)
                        <div class="heart pull-right heart-mar-right is-active" data-id = "{{$Ads->AdsId}}"></div>
                    @else
                        <div class="heart pull-right heart-mar-right" data-id = "{{$Ads->AdsId}}"></div>
                    @endif
                  </div> --}}
              <a href="/Listing_details/AdsDetails/{{$Ads->id}}/{{$Ads->AdsId}}">
                <img class="img-fluid home-img-mar-top" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
              </a>
            </figure>
            <div class="feature-content">
              <div class="product">
                <a href="/Listing_details/AdsDetails/{{$Ads->id}}/{{$Ads->AdsId}}">
                  <i class="lni-folder">
                  </i> {{$Ads->CategoryName}}
                </a>
              </div>
              <h4>

                <input type="hidden" id="PostId" value="{{$Ads->id}}">
                <a href="/Listing_details/AdsDetails/{{$Ads->id}}/{{$Ads->AdsId}}">{{$Ads->title}}
                </a>
              </h4>
              <span>Last Updated: @php
                $now = new DateTime;
$full = false;
$ago = new DateTime($Ads->created_at);
$diff = $now->diff($ago);

$diff->w = floor($diff->d / 7);
$diff->d -= $diff->w * 7;

$string = array(
'y' => 'year',
'm' => 'month',
'w' => 'week',
'd' => 'day',
'h' => 'hour',
'i' => 'minute',
's' => 'second',
);
foreach ($string as $k => &$v) {
if ($diff->$k) {
$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
} else {
unset($string[$k]);
}
}

if (!$full) $string = array_slice($string, 0, 1);
echo $string ? implode(', ', $string) . ' ago' : 'just now';
            @endphp
              </span>
              <ul class="address">
                <li class="width100">
                  <a href="/Listing_details/AdsDetails/{{$Ads->id}}/{{$Ads->AdsId}}">
                    <i class="lni-map-marker">
                    </i> {{$Ads->CityName}}
                  </a>
                </li>

                <li class="width100">
                  <a href="#">
                    <i class="lni-alarm-clock">
                    </i> {{date('d M Y', strtotime($Ads->created_at))}}
                  </a>
                </li>
                <li class="width100">
                  <a href="#">
                    <i class="lni-user">
                    </i> {{substr($Ads->UserName, 0, 6)}}
                  </a>
                </li>
                {{--  <li>
                  <a href="#">
                    <i class="lni-package">
                    </i> Used
                  </a>
                </li>  --}}
              </ul>
              <div class="listing-bottom">
                <h3 class="price float-left">{{$Ads->price}}
                </h3>
                {{--  <a href="#" class="btn-verified float-right">
                        @php
                        $now = new DateTime;
      $full = false;
      $ago = new DateTime($Ads->created_at);
      $diff = $now->diff($ago);

      $diff->w = floor($diff->d / 7);
      $diff->d -= $diff->w * 7;

      $string = array(
      'y' => 'year',
      'm' => 'month',
      'w' => 'week',
      'd' => 'day',
      'h' => 'hour',
      'i' => 'minute',
      's' => 'second',
      );
      foreach ($string as $k => &$v) {
      if ($diff->$k) {
      $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
      } else {
      unset($string[$k]);
      }
      }

      if (!$full) $string = array_slice($string, 0, 1);
      echo $string ? implode(', ', $string) . ' ago' : 'just now';
                    @endphp
                </a>  --}}
              </div>
            </div>
          </div>
        </div>
        @endforeach
    </div>
    </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 ads-mar-top" id="AdsRight">
            @foreach($GetRightAds as $Ads)
                    @if($Ads->ads_1)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link1 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_1")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif
                    <br>
                    @if($Ads->ads_2)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link2 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_2")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_3)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link3 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_3")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_4)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link4 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_4")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_5)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link5 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_5")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_6)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link6 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_6")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_7)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link7 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_7")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_8)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link8 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_8")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_9)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link9 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_9")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                    <br>
                    @if($Ads->ads_10)
                    <div class="text-center">
                        <a href="{{ $Ads->ads_link10 }}" target="_blank"> <img src="{{asset("HomeLeftImages/$Ads->ads_10")}}" class="ads-width" alt=""></a>
                    </div>
                    @endif

                @endforeach
        </div>
      </div>
    </div>
    {{--  @foreach($GetAds as $Ads)
            @if($Ads->home_top_featured_ads)
            <div class="text-center">
                <a href="{{ $Ads->home_top_featured_ads_link }}" target="_blank"> <img src="{{asset("AdsImage/$Ads->home_top_featured_ads")}}" alt=""></a>
            </div>
            @endif
        @endforeach  --}}
  </section>
  {{-- <section class="featured-lis section-padding">
    <div class="container">

      <div class="row">
        <div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
          <h3 class="section-title">Featured Products
          </h3>
          <div id="new-products" class="owl-carousel">
            @foreach($GetFeaturedAds as $FeaturedAds)
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img class="img-fluid" src="{{asset("UI/img/product/img1.jpg")}}" alt="">
                  <div class="overlay">
                  </div>
                  <div class="btn-product bg-yellow">
                    <a href="#">New
                    </a>
                  </div>
                </div>
                <div class="product-content">
                  <h3 class="product-title">
                    <a href="ads-details.html">{{$FeaturedAds->title}}
                    </a>
                  </h3>
                  <p>Lorem ipsum dolor sit
                  </p>
                  <span class="price">{{$FeaturedAds->price}}
                  </span>
                  <div class="meta">
                    <span class="icon-wrap">
                      <i class="lni-star-filled">
                      </i>
                      <i class="lni-star-filled">
                      </i>
                      <i class="lni-star-filled">
                      </i>
                      <i class="lni-star">
                      </i>
                      <i class="lni-star">
                      </i>
                    </span>
                    <span class="count-review">
                      <span>1
                      </span> Reviews
                    </span>
                  </div>
                  <div class="card-text">
                    <div class="float-left">
                      <a href="#">
                        <i class="lni-map-marker">
                        </i> {{$FeaturedAds->CityName}}
                      </a>
                    </div>
                    <div class="float-right">
                      <div class="icon">
                        <i class="lni-heart">
                        </i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section> --}}
  <section class="services section-padding">
    <div class="container">

      <div class="row">
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.2s">
            <div class="icon">
              <i class="lni-book">
              </i>
            </div>
            <div class="services-content">
              <h3>
                <a href="#">Fully Documented
                </a>
              </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.4s">
            <div class="icon">
              <i class="lni-leaf">
              </i>
            </div>
            <div class="services-content">
              <h3>
                <a href="#">Clean & Modern Design
                </a>
              </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.6s">
            <div class="icon">
              <i class="lni-map">
              </i>
            </div>
            <div class="services-content">
              <h3>
                <a href="#">Great Features
                </a>
              </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="0.8s">
            <div class="icon">
              <i class="lni-cog">
              </i>
            </div>
            <div class="services-content">
              <h3>
                <a href="#">Completely Customizable
                </a>
              </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="1s">
            <div class="icon">
              <i class="lni-pointer-up">
              </i>
            </div>
            <div class="services-content">
              <h3>
                <a href="#">User Friendly
                </a>
              </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xs-12">
          <div class="services-item wow fadeInRight" data-wow-delay="1.2s">
            <div class="icon">
              <i class="lni-layout">
              </i>
            </div>
            <div class="services-content">
              <h3>
                <a href="#">Awesome Layout
                </a>
              </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere.
              </p>
            </div>
          </div>
        </div>
      </div>
      {{--  @foreach($GetAds as $Ads)
            @if($Ads->home_top_bottom_ads)
            <div class="text-center">
                <a href="{{ $Ads->home_top_bottom_ads_link }}" target="_blank"> <img src="{{asset("AdsImage/$Ads->home_top_bottom_ads")}}" alt=""></a>
            </div>
            @endif
        @endforeach  --}}
    </div>
  </section>
  {{-- <section class="cta section-padding">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4">
          <div class="single-cta">
            <div class="cta-icon">
              <i class="lni-grid">
              </i>
            </div>
            <h4>Refreshing Design
            </h4>
            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
            </p>
          </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
          <div class="single-cta">
            <div class="cta-icon">
              <i class="lni-brush">
              </i>
            </div>
            <h4>Easy to Customize
            </h4>
            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
            </p>
          </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
          <div class="single-cta">
            <div class="cta-icon">
              <i class="lni-headphone-alt">
              </i>
            </div>
            <h4>24/7 Support
            </h4>
            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
            </p>
          </div>
        </div>
      </div>
    </div>
  </section> --}}
@endsection
