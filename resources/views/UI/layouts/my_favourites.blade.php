@extends('UI.base')

@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-wrapper">
          <h2 class="product-title">My Favourites
          </h2>
          <ol class="breadcrumb">
            <li>
              <a href="#">Home /
              </a>
            </li>
            <li class="current">My Favourites
            </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="content" class="section-padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
        <aside>
          <div class="sidebar-box">
            @include('UI.common.user_sidebar')
          </div>
          <div class="widget">
            <h4 class="widget-title">Advertisement
            </h4>
            <div class="add-box">
              <img class="img-fluid" src="assets/img/img1.jpg" alt="">
            </div>
          </div>
        </aside>
      </div>
      <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="page-content">
          <div class="inner-box">
            <div class="dashboard-box">
              <h2 class="dashbord-title">My Favourites
              </h2>
            </div>
            <div class="dashboard-wrapper">
              <table class="table table-responsive dashboardtable tablemyads">
                <thead>
                  <tr>
                    <th>Photo
                    </th>
                    <th>Title
                    </th>
                    <th>Category
                    </th>
                    <th>Ad Status
                    </th>
                    <th>Price
                    </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($GetMyFavAds as $Ads)
                  <tr data-category="active">
                    <td class="photo">
                      <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
                    </td>
                    <td data-title="Title">
                      <h3>{{$Ads->title}}
                      </h3>
                      <span>Ad ID: {{$Ads->ads_id}}
                      </span>
                    </td>
                    <td data-title="Category">
                      <span class="adcategories">{{$Ads->name}}
                      </span>
                    </td>
                    <td data-title="Ad Status">

                        @if($Ads->status == 1)
                            <span class="adstatus adstatusactive">
                            Active
                            </span>
                        @else
                            <span class="adstatus adstatusinactive">
                            In active
                            </span>
                        @endif

                    </td>
                    <td data-title="Price">
                      <h3>${{$Ads->price}}
                      </h3>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
