@extends('UI.base')
@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-wrapper">
          <h2 class="product-title">Profile Settings
          </h2>
          <ol class="breadcrumb">
            <li>
              <a href="#">Home /
              </a>
            </li>
            <li class="current">Profile Settings
            </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="content" class="section-padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
        <aside>
          <div class="sidebar-box">
            @include('UI.common.user_sidebar')
          </div>
          <div class="widget">
            <h4 class="widget-title">Advertisement
            </h4>
            <div class="add-box">
              <img class="img-fluid" src="assets/img/img1.jpg" alt="">
            </div>
          </div>
        </aside>
      </div>
      <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="row page-content">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="inner-box">
              <div class="dashboard-box">
                <h2 class="dashbord-title">Ad Detail
                </h2>
              </div>
              <div class="dashboard-wrapper" id="PostDatas">
                <div class="form-group mb-3">
                  <label class="control-label">Project Title
                  </label>
                  <input class="form-control input-md" name="Title" id="AdsId" type="hidden" value="{{$GetPosts->id}}" placeholder="Title" type="text">
                </div>
                <input class="form-control input-md" name="Title" id="title" value="{{$GetPosts->title}}" placeholder="Title" type="text">
                </div>
                <div class="form-group mb-3 tg-inputwithicon">
                  <label class="control-label">Categories
                  </label>
                  <div class="tg-select form-control">
                    <select id="categories" onchange="CheckSubCategories()">
                      @foreach($GetCategories as $Categories)
                      <option value="{{$Categories->translation_of}}" {{$Categories->translation_of == $GetPosts->category_id  ? 'selected' : ''}}>{{$Categories->name}}
                      </option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group mb-3 tg-inputwithicon" id="SubCategoriesShow">
                  <label class="control-label">Sub Categories
                  </label>
                  <div class="tg-select form-control">
                    <select id="SubCategories">
                        @foreach($GetSubcategories as $Categories)
                        <option value="{{$Categories->parent_id}}" {{$Categories->parent_id == $GetPosts->sub_category_id  ? 'selected' : ''}}>{{$Categories->name}}
                        </option>
                        @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group mb-3">
                  <label class="control-label">Price
                  </label>
                  <input class="form-control input-md" name="price" id="price" value="{{$GetPosts->price}}" placeholder="Ad your Price" type="text">
                  <div class="tg-checkbox mt-3">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" value="" id="tg-priceoncall">
                      <label class="custom-control-label" for="tg-priceoncall">Price On Call
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group md-3">
                  {{-- <section id="editor">
                    <div id="summernote">
                    </div>
                  </section> --}}
                  <textarea name="post_description">{{$GetPosts->description}}</textarea>
                </div>
                <div class="form-group mb-3 tg-inputwithicon">
                    <label class="control-label">City
                    </label>
                    <div class="tg-select form-control">
                      <select id="city">
                        @foreach($GetCities as $Cities)
                        <option value="{{$Cities->id}}" {{$Cities->id == $GetPosts->city_id  ? 'selected' : ''}}>{{$Cities->name}}</option>
                      @endforeach
                      </select>
                    </div>
                  </div>
                <div class="form-group mb-3">
                  <label class="control-label">Tags
                  </label>
                  <input class="form-control input-md" name="price" id="tags" placeholder="Tags" type="text">
                </div>

                <div class="form-group mb-3 width50 pull-left">
                    <label class="control-label">Upload featured image
                    </label>
                    <input class="form-control input-md" name="price" id="file" placeholder="Tags" type="file">
                  </div>

                  <div class="width50 pull-left featured-img-pad-left">
                    <img class="featured-img-width" src="{{URL::asset('Post_Featured_image')}}/{{$GetFeaturedImage->filename}}" alt="">
                  </div>
                {{-- <form method="post" action="{{url('image/upload/store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone"> --}}

                {{-- <div id="dropzone"></div> --}}

                <!-- <button class="btn btn-common" type="button" id="PostAds">Save and continue
                </button> -->
              </div>
              <div class="dashboard-wrapper ShoworHideDropImages">
                <div class="form-group mb-3 ">
                    <label class="control-label">Upload Multiple Images
                      </label>
                    <form method="post" action="{{url('image/upload/updatestore')}}" enctype="multipart/form-data" class="dropzone" id="dropzone" >
                        <input class="form-control input-md" name="AdsId" type="hidden" value="{{$GetPosts->id}}" placeholder="Title" type="text">
                      @csrf
                  </form>

                  <div class="row" id="PostImages">
                    @foreach($GetPostImages as $PostImages)
                    <div class="col-md-3">

                            <img class="post_images" src="{{URL::asset('public/Post_images/')}}/{{$PostImages->post_images}}" alt="">
                            <a href="javascript:void(0);" onclick="DeletePostimages({{$PostImages->id}})">Delete</a>
                    </div>

                  @endforeach
                </div>

                  <br>
                  <button class="btn btn-common" id="UpdateAds" type="button" id="">Post Ad
                    </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


@section('JSscript')
   <script>
//       Dropzone.options.dropzone =
//          {
//             maxFilesize: 2,
//             renameFile: function(file) {
//                 var dt = new Date();
//                 var time = dt.getTime();
//                return time+file.name;
//             },
//             acceptedFiles: ".jpeg,.jpg,.png,.gif",
//             addRemoveLinks: true,
//             timeout: 50000,
//             removedfile: function(file)
//             {
//                 var name = file.upload.filename;
//                 $.ajax({
//                     headers: {
//                                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//                             },
//                     type: 'POST',
//                     url: '{{ url("image/delete") }}',
//                     data: {filename: name},
//                     success: function (data){
//                         console.log("File has been successfully removed!!");
//                     },
//                     error: function(e) {
//                         console.log(e);
//                     }});
//                     var fileRef;
//                     return (fileRef = file.previewElement) != null ?
//                     fileRef.parentNode.removeChild(file.previewElement) : void 0;
//             },

//             success: function(file, response)
//             {
//                 console.log(response);
//             },
//             error: function(file, response)
//             {
//                return false;
//             }
// };
</script>
@endsection
