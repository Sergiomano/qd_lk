@extends('UI.base')
@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumb-wrapper">
          <h2 class="product-title">Dashbord
          </h2>
          <ol class="breadcrumb">
            <li>
              <a href="#">Home /
              </a>
            </li>
            <li class="current">Dashboard
            </li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="content" class="section-padding">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
        <aside>
          <div class="sidebar-box">
            @include('UI.common.user_sidebar')
          </div>
          <div class="widget">
            <h4 class="widget-title">Advertisement
            </h4>
            <div class="add-box">
              <img class="img-fluid" src="assets/img/img1.jpg" alt="">
            </div>
          </div>
        </aside>
      </div>
      <div class="col-sm-12 col-md-8 col-lg-9">
        <div class="page-content">
            @foreach($GetHomeAdsAds as $Ads)
                @if($Ads->home_top_ads)
                <div class="text-center">
                    <img src="{{asset("AdsImage/$Ads->home_top_ads")}}" alt="">
                </div>
                @endif
            @endforeach
          <div class="inner-box">
            <div class="dashboard-box">
              <h2 class="dashbord-title">Dashboard
              </h2>
            </div>
            <div class="dashboard-wrapper">
              {{--  <div class="dashboard-sections">
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                    <div class="dashboardbox">
                      <div class="icon">
                        <i class="lni-write">
                        </i>
                      </div>
                      <div class="contentbox">
                        <h2>
                          <a href="#">Total Ad Posted
                          </a>
                        </h2>
                        <h3>{{count($GetAds)}} Ads Posted
                        </h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                    <div class="dashboardbox">
                      <div class="icon">
                        <i class="lni-add-files">
                        </i>
                      </div>
                      <div class="contentbox">
                        <h2>
                          <a href="#">Featured Ads
                          </a>
                        </h2>
                        <h3>{{count($GetFeaturedAds)}} Ads Posted
                        </h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
                    <div class="dashboardbox">
                      <div class="icon">
                        <i class="lni-support">
                        </i>
                      </div>
                      <div class="contentbox">
                        <h2>
                          <a href="#">Offers / Messages
                          </a>
                        </h2>
                        <h3>2040 Messages
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  --}}
              <table class="table table-responsive dashboardtable tablemyads">
                <thead>
                  <tr>
                    <th>Photo
                    </th>
                    <th>Title
                    </th>
                    <th>Category
                    </th>
                    <th>Ad Status
                    </th>
                    <th>Price
                    </th>
                    <th>Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($GetAds as $Ads)
                  <tr data-category="active">
                    <td class="photo">
                      <img class="img-fluid" src="{{asset("Post_Featured_image/$Ads->filename")}}" alt="">
                    </td>
                    <td data-title="Title">
                      <h3>{{$Ads->title}}
                      </h3>
                      <span>Ad ID: {{$Ads->ads_id}}
                      </span>
                    </td>
                    <td data-title="Category">
                      <span class="adcategories">{{$Ads->name}}
                      </span>
                    </td>
                    <td data-title="Ad Status">

                        @if($Ads->status == 0)
                            <span class="adstatus adstatusactive">
                            Active
                            </span>
                        @else
                            <span class="adstatus adstatusinactive">
                            In active
                            </span>
                        @endif

                    </td>
                    <td data-title="Price">
                      <h3>${{$Ads->price}}
                      </h3>
                    </td>
                    <td data-title="Action">
                      <div class="btns-actions">
                        <a class="btn-action btn-view" target="_blank" href="/Listing_details/AdsDetails/{{$Ads->category_id}}/{{$Ads->id}}">
                          <i class="lni-eye">
                          </i>
                        </a>
                        <a class="btn-action btn-edit" href="/EditPosts/{{$Ads->id}}">
                          <i class="lni-pencil">
                          </i>
                        </a>
                        <a class="btn-action btn-delete" href="#">
                          <i class="lni-trash">
                          </i>
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
