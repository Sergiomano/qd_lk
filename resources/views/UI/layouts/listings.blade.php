@extends('UI.base')
@section('Content')
<div class="page-header" style="background: url(assets/img/banner1.jpg);">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="breadcrumb-wrapper">
            <h2 class="product-title">Listings
            </h2>
            <ol class="breadcrumb">
              <li>
                <a href="#">Home /
                </a>
              </li>
              <li class="current">Listings
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main-container section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @foreach($GetAds as $Ads)
                    @if($Ads->home_top_ads)
                    <div class="text-center">
                        <img src="{{asset("AdsImage/$Ads->home_top_ads")}}" alt="">
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
      <div class="row">
        <div class="col-lg-3 col-md-12 col-xs-12 page-sidebar">
          <aside>
            <div class="widget categories">
              <h4 class="widget-title">All Categories
              </h4>
              <ul class="categories-list">
                @foreach($GetCategories as $Categories)
                <li>
                  <a href="/Categories/{{$Categories->id}}">
                    <i class="lni-dinner">
                    </i>
                    {{$Categories->name}}
                    <span class="category-counter">
                    </span>
                  </a>
                </li>
                @endforeach
              </ul>
            </div>
            <div class="widget">
              <h4 class="widget-title">Advertisement
              </h4>
              <div class="add-box">
                <img class="img-fluid" src="assets/img/img1.jpg" alt="">
              </div>
            </div>
          </aside>
        </div>
        <div class="col-lg-9 col-md-12 col-xs-12 page-content">

          <div class="product-filter">
            <div class="short-name">
              {{-- <span>Showing (1 - 12 products of 7371 products)
              </span> --}}
            </div>
            <div id="list-view" class="tab-pane fade active show">
                    <div class="text-center">
                          <img src="{{asset("UI/ajax_loader.gif")}}" class="loader-center" id="AjaxLoader" style="display:none;" alt="">
                    </div>

                  <div class="row">

                    @foreach($GetPaymentPosts as $Posts)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="featured-box payment-ads-bg">
                        <figure>
                          {{-- <div class="icon">
                            <i class="lni-heart">
                            </i>
                          </div> --}}
                          <a href="#">
                              <img class="img-fluid" src="{{asset("Pay_Featured_Image")}}/{{$Posts->filename}}" alt="">
                          </a>
                        </figure>
                        <div class="feature-content">
                          <div class="product">
                            <a href="#">
                              <i class="lni-folder">
                              </i> {{$Posts->name}}
                            </a>
                          </div>
                          <h4>
                            <a href="/Listing_details/PaidAdsDetails/{{$Posts->category_id}}/{{$Posts->id}}">{{$Posts->title}}
                            </a>
                          </h4>
                          <span>Last Updated: @php
                                  $now = new DateTime;
                $full = false;
                $ago = new DateTime($Posts->created_at);
                $diff = $now->diff($ago);

                $diff->w = floor($diff->d / 7);
                $diff->d -= $diff->w * 7;

                $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
                );
                foreach ($string as $k => &$v) {
                if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                unset($string[$k]);
                }
                }

                if (!$full) $string = array_slice($string, 0, 1);
                echo $string ? implode(', ', $string) . ' ago' : 'just now';
                              @endphp
                          </span>
                          <ul class="address">
                            <li>
                              <a href="#">
                                <i class="lni-map-marker">
                                </i>{{$Posts->address}}, {{$Posts->CityName}}
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="lni-alarm-clock">
                                </i> {{date("M d Y", strtotime($Posts->created_at))}}
                              </a>
                            </li>
                            {{--  <li>
                              <a href="#">
                                <i class="lni-user">
                                </i> {{$Posts->UserName}}
                              </a>
                            </li>  --}}
                            {{--  <li>
                              <a href="#">
                                <i class="lni-package">
                                </i> Brand New
                              </a>
                            </li>  --}}
                          </ul>
                          <div class="listing-bottom">
                            <h3 class="price float-left">${{$Posts->price}}
                            </h3>

                        <img src="{{URL::asset('UI/badge.png')}}" class="badge-ads" alt="">
                            {{-- <a href="account-myads.html" class="btn-verified float-right">

                            </a> --}}
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                </div>
            <div class="Show-item">
              <span>Show Items
              </span>
              <form class="woocommerce-ordering" method="post">
                <label>
                  <select name="order" id="sortby" onchange="ChangeSortBy()" class="orderby">
                    <option value="" selected disabled>Sort by
                    </option>
                    <option value="1">High to low
                    </option>
                    <option value="2">Low to high
                    </option>
                    <option value="3">Newest to oldest
                    </option>
                    <option value="4">Oldest to newest
                    </option>
                  </select>
                </label>
              </form>
            </div>
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#grid-view">
                  <i class="lni-grid">
                  </i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#list-view">
                  <i class="lni-list">
                  </i>
                </a>
              </li>
            </ul>
          </div>
          <div class="adds-wrapper">
            <div class="tab-content">
              <div id="grid-view" class="tab-pane fade">
                <div class="row">
                @foreach($GetPosts as $Posts)
                  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="featured-box">
                      <figure>
                        <a href="#">
                            <img class="img-fluid" src="{{asset("Post_Featured_image")}}/{{$Posts->featured_images}}" alt="">
                        </a>
                      </figure>
                      <div class="feature-content">
                        <div class="product">
                          <a href="#">
                            <i class="lni-folder">
                            </i> {{$Posts->name}}
                          </a>
                        </div>
                        <h4>
                          <a href="#">{{$Posts->title}}
                          </a>
                        </h4>
                        <span>Last Updated: @php
                                $now = new DateTime;
              $full = false;
              $ago = new DateTime($Posts->created_at);
              $diff = $now->diff($ago);

              $diff->w = floor($diff->d / 7);
              $diff->d -= $diff->w * 7;

              $string = array(
              'y' => 'year',
              'm' => 'month',
              'w' => 'week',
              'd' => 'day',
              'h' => 'hour',
              'i' => 'minute',
              's' => 'second',
              );
              foreach ($string as $k => &$v) {
              if ($diff->$k) {
              $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
              } else {
              unset($string[$k]);
              }
              }

              if (!$full) $string = array_slice($string, 0, 1);
              echo $string ? implode(', ', $string) . ' ago' : 'just now';
                            @endphp
                        </span>
                        <ul class="address">
                          <li>
                            <a href="#">
                              <i class="lni-map-marker">
                              </i> {{$Posts->address}}, {{$Posts->CityName}}
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="lni-alarm-clock">
                              </i> {{date("M d Y", strtotime($Posts->created_at))}}
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="lni-user">
                              </i> {{$Posts->UserName}}
                            </a>
                          </li>
                          {{-- <li>
                            <a href="#">
                              <i class="lni-package">
                              </i> Used
                            </a>
                          </li> --}}
                        </ul>
                        <div class="listing-bottom">
                          <h3 class="price float-left">{{$Posts->price}}
                          </h3>
                          {{-- <a href="account-myads.html" class="btn-verified float-right">
                            <i class="lni-check-box">
                            </i> Verified Ad
                          </a> --}}
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
              <div id="list-view" class="tab-pane fade active show">
                  <div class="text-center">
                        <img src="{{asset("UI/ajax_loader.gif")}}" class="loader-center" id="AjaxLoader" style="display:none;" alt="">
                  </div>

                <div class="row" id="AdsResults">

                  @foreach($GetPosts as $Posts)
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="featured-box">
                      <figure>
                        {{-- <div class="icon">
                          <i class="lni-heart">
                          </i>
                        </div> --}}
                        <a href="#">
                            <img class="img-fluid" src="{{asset("Post_Featured_image")}}/{{$Posts->featured_images}}" alt="">
                        </a>
                      </figure>
                      <div class="feature-content">
                        <div class="product">
                          <a href="#">
                            <i class="lni-folder">
                            </i> {{$Posts->name}}
                          </a>
                        </div>
                        <h4>
                          <a href="/Listing_details/AdsDetails/{{$Posts->category_id}}/{{$Posts->id}}">{{$Posts->title}}
                          </a>
                        </h4>
                        <span>Last Updated: @php
                                $now = new DateTime;
              $full = false;
              $ago = new DateTime($Posts->created_at);
              $diff = $now->diff($ago);

              $diff->w = floor($diff->d / 7);
              $diff->d -= $diff->w * 7;

              $string = array(
              'y' => 'year',
              'm' => 'month',
              'w' => 'week',
              'd' => 'day',
              'h' => 'hour',
              'i' => 'minute',
              's' => 'second',
              );
              foreach ($string as $k => &$v) {
              if ($diff->$k) {
              $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
              } else {
              unset($string[$k]);
              }
              }

              if (!$full) $string = array_slice($string, 0, 1);
              echo $string ? implode(', ', $string) . ' ago' : 'just now';
                            @endphp
                        </span>
                        <ul class="address">
                          <li>
                            <a href="#">
                              <i class="lni-map-marker">
                              </i>{{$Posts->address}}, {{$Posts->CityName}}
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="lni-alarm-clock">
                              </i> {{date("M d Y", strtotime($Posts->created_at))}}
                            </a>
                          </li>
                          <li>
                            <a href="#">
                              <i class="lni-user">
                              </i> {{$Posts->UserName}}
                            </a>
                          </li>
                          {{--  <li>
                            <a href="#">
                              <i class="lni-package">
                              </i> Brand New
                            </a>
                          </li>  --}}
                        </ul>
                        <div class="listing-bottom">
                          <h3 class="price float-left">${{$Posts->price}}
                          </h3>
                          {{-- <a href="account-myads.html" class="btn-verified float-right">

                          </a> --}}
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
          <div class="pagination-bar pull-right">
            {{-- <nav>
              <ul class="pagination justify-content-center">
                <li class="page-item">
                  <a class="page-link active" href="#">1
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">2
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">3
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">Next
                  </a>
                </li>
              </ul>
            </nav> --}}
            {{ $GetPosts->onEachSide(1)->links() }}

            {{-- @if ($GetPosts->hasMorePages())
            <li><a href="{{ $GetPosts->nextPageUrl() }}" rel="next">Next ?</a></li>
        @else
            <li class="disabled"><span>Next ?</span></li>
        @endif --}}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
