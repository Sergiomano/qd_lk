<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>{{$title}}</title>
    <!-- chartist CSS -->
    <link href="{{asset("Admin/assets/libs/chartist/dist/chartist.min.css")}}" rel="stylesheet">
    <link href="{{asset("Admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css")}}" rel="stylesheet">
    <link href="{{asset("Admin/dist/css/style.css")}}" rel="stylesheet">
    <link href="{{asset("Admin/dist/css/custom.css")}}" rel="stylesheet">
    <!-- This page CSS -->
    <link href="{{asset("Admin/dist/css/pages/dashboard1.css")}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="{{asset("Admin/assets/libs/sweetalert2/dist/sweetalert2.min.css")}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://www.dropzonejs.com/css/dropzone.css?v=1533562669">
</head>

<body>
    <div class="main-wrapper" id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="loader">
                <div class="loader__figure"></div>
                <p class="loader__label">Material Admin</p>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        @include('Admin.common.header')
        <!-- ============================================================== -->
        <!-- Sidebar scss in sidebar.scss -->
        <!-- ============================================================== -->
        @include('Admin.common.left_sidebar')
        <!-- ============================================================== -->
        <!-- Sidebar scss in sidebar.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        @yield('Content')
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->


        @include('Admin.common.right_sidebar')
        <div class="chat-windows"></div>
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->

    <!-- All Required js -->
    <!-- ============================================================== -->
    <script data-cfasync="false" src="../../../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="{{asset("Admin/assets/libs/jquery/dist/jquery.min.js")}}"></script>
    <script src="{{asset("Admin/dist/js/materialize.min.js")}}"></script>
    <script src="{{asset("Admin/assets/libs/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js")}}"></script>

    {{-- Dropzone --}}
    <script src="https://www.dropzonejs.com/js/dropzone.js?v=1533562669"></script>


    <!-- ============================================================== -->
    <!-- Apps -->
    <!-- ============================================================== -->
    <script src="{{asset("Admin/dist/js/app.js")}}"></script>
    <script src="{{asset("Admin/dist/js/app.init.js")}}"></script>
    <script src="{{asset("Admin/dist/js/app-style-switcher.js")}}"></script>
    <!-- ============================================================== -->
    <!-- Custom js -->
    <!-- ============================================================== -->
    <script src="{{asset("Admin/dist/js/custom.min.js")}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script src="{{asset("Admin/assets/libs/chartist/dist/chartist.min.js")}}"></script>
    <script src="{{asset("Admin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js")}}"></script>
    <script src="{{asset("Admin/assets/extra-libs/sparkline/sparkline.js")}}"></script>
    <script src="{{asset("Admin/dist/js/pages/dashboards/dashboard1.js")}}"></script>

    <script src="{{asset("Admin/dist/js/custom/attributes.js")}}"></script>

    <script src="{{asset("Admin/assets/libs/sweetalert2/dist/sweetalert2.min.js")}}"></script>
    <script src="{{asset("Admin/assets/libs/sweetalert2/sweet-alert.init.js")}}"></script>



    {{-- CK Editor --}}
    <script src="https://cdn.ckeditor.com/4.9.2/basic/ckeditor.js"></script>

    <script>
			CKEDITOR.replace( 'post_description' );
    </script>

</body>


</html>
