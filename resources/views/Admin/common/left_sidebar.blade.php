<aside class="left-sidebar">
    <ul id="slide-out" class="sidenav">

        <li>
            <ul class="collapsible collapsiblebox">
                <li class="small-cap"><span class="hide-menu">PERSONAL</span></li>
                <li>
                    <a href="javascript: void(0);"><i class="material-icons">dashboard</i><span class="hide-menu"> Dashboard</span></a>
                </li>
                 {{--  <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">equalizer</i><span class="hide-menu"> Sidebar Types </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="sidebar-type-minisidebar.html"><i class="material-icons">photo_size_select_small</i><span class="hide-menu">Minisidebar</span></a></li>
                            <li><a href="sidebar-type-iconbar.html"><i class="material-icons">picture_in_picture</i><span class="hide-menu">Icon Sidebar</span></a></li>
                            <li><a href="sidebar-type-overlay.html"><i class="material-icons">low_priority</i><span class="hide-menu">Overlay Sidebar</span></a></li>
                            <li><a href="sidebar-type-fullsidebar.html"><i class="material-icons">present_to_all</i><span class="hide-menu">Full Sidebar</span></a></li>
                            <li><a href="sidebar-type-horizontalsidebar.html"><i class="material-icons">power_input</i><span class="hide-menu">Horizontal Sidebar</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">library_books</i><span class="hide-menu"> Page Layouts </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="layout-inner-fixed-left-sidebar.html"><i class="material-icons">format_align_left</i><span class="hide-menu">Inner Fixed Left Sidebar</span></a></li>
                            <li><a href="layout-inner-fixed-right-sidebar.html"><i class="material-icons">format_align_right</i><span class="hide-menu">Inner Fixed Right Sidebar</span></a></li>
                            <li><a href="layout-inner-left-sidebar.html"><i class="material-icons">format_indent_increase</i><span class="hide-menu">Inner Left Sidebar</span></a></li>
                            <li><a href="layout-inner-right-sidebar.html"><i class="material-icons">format_indent_decrease</i><span class="hide-menu">Inner Right Sidebar</span></a></li>
                            <li><a href="page-layout-fixed-header.html"><i class="material-icons">line_weight</i><span class="hide-menu">Fixed Header</span></a></li>
                            <li><a href="page-layout-fixed-sidebar.html"><i class="material-icons">line_weight</i><span class="hide-menu">Fixed Sidebar</span></a></li>
                            <li><a href="page-layout-fixed-header-sidebar.html"><i class="material-icons">format_align_center</i><span class="hide-menu">Fixed Header & Sidebar</span></a></li>
                            <li><a href="page-layout-boxed-layout.html"><i class="material-icons">format_line_spacing</i><span class="hide-menu">Boxed Layout</span></a></li>
                        </ul>
                    </div>
                </li>  --}}
                <li class="small-cap"><span class="hide-menu">Payment Ads</span></li>
                <li>
                    <a href="/Admin/AdsList"><i class="material-icons">move_to_inbox</i><span class="hide-menu"> List of ads</span></a>
                </li>

                <li>
                    <a href="/Admin/AddAds"><i class="material-icons">move_to_inbox</i><span class="hide-menu"> Add new ads</span></a>
                </li>

                <li class="small-cap"><span class="hide-menu">Advertisements</span></li>
                <li>
                    <a href="/HomepageAds"><i class="material-icons">move_to_inbox</i><span class="hide-menu"> Home Page Ads</span></a>
                </li>

                <li>
                    <a href="/HomepageLeftAds"><i class="material-icons">move_to_inbox</i><span class="hide-menu"> Home Page Left Ads</span></a>
                </li>

                <li>
                    <a href="/HomepageRightAds"><i class="material-icons">move_to_inbox</i><span class="hide-menu"> Home Page Right Ads</span></a>
                </li>

                <li class="small-cap"><span class="hide-menu">Cities</span></li>
                <li>
                    <a href="/Admin/Cities"><i class="material-icons">move_to_inbox</i><span class="hide-menu"> Cities list</span></a>
                </li>
                {{--  <li class="small-cap"><span class="hide-menu">UI</span></li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">widgets</i><span class="hide-menu"> UI Elements </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="ui-badge.html"><i class="material-icons">crop_3_2</i><span class="hide-menu">Badges</span></a></li>
                            <li><a href="ui-button.html"><i class="material-icons">hdr_strong</i><span class="hide-menu">Buttons</span></a></li>
                            <li><a href="ui-breadcrumbs.html"><i class="material-icons">drag_handle</i><span class="hide-menu">Breadcrumbs</span></a></li>
                            <li><a href="ui-collections.html"><i class="material-icons">nfc</i><span class="hide-menu">Collections</span></a></li>
                            <li><a href="ui-floating-action-button.html"><i class="material-icons">panorama_horizontal</i><span class="hide-menu">Floating Action Button</span></a></li>
                            <li><a href="ui-footer.html"><i class="material-icons">video_label</i><span class="hide-menu">Footer</span></a></li>
                            <li><a href="ui-navbar.html"><i class="material-icons">view_compact</i><span class="hide-menu">Navbar</span></a></li>
                            <li><a href="ui-pagination.html"><i class="material-icons">swap_horiz</i><span class="hide-menu">Pagination</span></a></li>
                            <li><a href="ui-preloader.html"><i class="material-icons">sync</i><span class="hide-menu">Preloader</span></a></li>
                            <li><a href="ui-sweetalert.html"><i class="material-icons">filter_vintage</i><span class="hide-menu">Sweetalert</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">credit_card</i><span class="hide-menu"> Cards </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="ui-cards.html"><i class="material-icons">layers</i><span class="hide-menu">Basic Cards</span></a></li>
                            <li><a href="ui-card-customs.html"><i class="material-icons">card_membership</i><span class="hide-menu">Custom Cards</span></a></li>
                            <li><a href="ui-card-draggable.html"><i class="material-icons">card_giftcard</i><span class="hide-menu">Draggable Cards</span></a></li>
                            <li><a href="ui-card-weather.html"><i class="material-icons">grain</i><span class="hide-menu">Weather Cards</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">compare</i><span class="hide-menu"> Components </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="component-noui-slider.html"><i class="material-icons">compare_arrows</i><span class="hide-menu">Noui Silder</span></a></li>
                            <li><a href="component-toastr.html"><i class="material-icons">cast</i><span class="hide-menu">Toastr</span></a></li>
                            <li><a href="component-rating.html"><i class="material-icons">star_half</i><span class="hide-menu">Rating</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">dns</i><span class="hide-menu"> Advanced UI </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="advanced-ui-carousel.html"><i class="material-icons">view_carousel</i><span class="hide-menu">Carousel</span></a></li>
                            <li><a href="advanced-ui-collapsible.html"><i class="material-icons">view_day</i><span class="hide-menu">Collapsible</span></a></li>
                            <li><a href="advanced-ui-dropdown.html"><i class="material-icons">vertical_align_bottom</i><span class="hide-menu">Dropdown</span></a></li>
                            <li><a href="advanced-ui-feature-discovery.html"><i class="material-icons">videogame_asset</i><span class="hide-menu">Feature Discovery</span></a></li>
                            <li><a href="advanced-ui-media.html"><i class="material-icons">video_library</i><span class="hide-menu">Media</span></a></li>
                            <li><a href="advanced-ui-modal.html"><i class="material-icons">vignette</i><span class="hide-menu">Modals</span></a></li>
                            <li><a href="advanced-ui-parallax.html"><i class="material-icons">view_day</i><span class="hide-menu">Parallax</span></a></li>
                            <li><a href="advanced-ui-pushpin.html"><i class="material-icons">wrap_text</i><span class="hide-menu">Pushpin</span></a></li>
                            <li><a href="advanced-ui-scrollspy.html"><i class="material-icons">theaters</i><span class="hide-menu">Scrollspy</span></a></li>
                            <li><a href="advanced-ui-sidenav.html"><i class="material-icons">web</i><span class="hide-menu">Sidenav</span></a></li>
                            <li><a href="advanced-ui-tabs.html"><i class="material-icons">short_text</i><span class="hide-menu">Tabs</span></a></li>
                            <li><a href="advanced-ui-toasts.html"><i class="material-icons">tab_unselected</i><span class="hide-menu">Toast</span></a></li>
                            <li><a href="advanced-ui-tooltips.html"><i class="material-icons">spa</i><span class="hide-menu">Tooltips</span></a></li>
                            <li><a href="advanced-ui-waves.html"><i class="material-icons">polymer</i><span class="hide-menu">Waves</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">brightness_7</i><span class="hide-menu"> CSS </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="ui-color.html"><i class="material-icons">looks</i><span class="hide-menu">Color</span></a></li>
                            <li><a href="ui-grid.html"><i class="material-icons">view_module</i><span class="hide-menu">Grid</span></a></li>
                            <li><a href="ui-helper.html"><i class="material-icons">live_help</i><span class="hide-menu">Helpers</span></a></li>
                            <li><a href="ui-media-css.html"><i class="material-icons">live_tv</i><span class="hide-menu">Media</span></a></li>
                            <li><a href="ui-pulse.html"><i class="material-icons">linear_scale</i><span class="hide-menu">Pulse</span></a></li>
                            <li><a href="ui-sass.html"><i class="material-icons">group_work</i><span class="hide-menu">Sass</span></a></li>
                            <li><a href="ui-shadow.html"><i class="material-icons">graphic_eq</i><span class="hide-menu">Shadow</span></a></li>
                            <li><a href="ui-transitions.html"><i class="material-icons">gesture</i><span class="hide-menu">Transitions</span></a></li>
                            <li><a href="ui-typography.html"><i class="material-icons">format_shapes</i><span class="hide-menu">Typography</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="small-cap"><span class="hide-menu">FORMS</span></li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">receipt</i><span class="hide-menu"> Form Elements </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="form-autocomplete.html"><i class="material-icons">format_indent_increase</i><span class="hide-menu">Autocomplete</span></a></li>
                            <li><a href="form-checkboxes.html"><i class="material-icons">check_circle</i><span class="hide-menu">Checkboxes</span></a></li>
                            <li><a href="form-chips.html"><i class="material-icons">collections_bookmark</i><span class="hide-menu">Chips</span></a></li>
                            <li><a href="form-pickers.html"><i class="material-icons">colorize</i><span class="hide-menu">Pickers</span></a></li>
                            <li><a href="form-radio-buttons.html"><i class="material-icons">album</i><span class="hide-menu">Radio Buttons</span></a></li>
                            <li><a href="form-range.html"><i class="material-icons">compare</i><span class="hide-menu">Range</span></a></li>
                            <li><a href="form-select.html"><i class="material-icons">check_box</i><span class="hide-menu">Select</span></a></li>
                            <li><a href="form-switch.html"><i class="material-icons">confirmation_number</i><span class="hide-menu">Switch</span></a></li>
                            <li><a href="form-text-inputs.html"><i class="material-icons">flash_auto</i><span class="hide-menu">Text Input</span></a></li>
                            <li><a href="form-inputs.html"><i class="material-icons">call_to_action</i><span class="hide-menu">Form Inputs</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">content_copy</i><span class="hide-menu"> Form Pages </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="form-masks.html"><i class="material-icons">filter_none</i><span class="hide-menu">Form Mask & Typehead</span></a></li>
                            <li><a href="form-layouts.html"><i class="material-icons">filter_b_and_w</i><span class="hide-menu">Form Layout</span></a></li>
                            <li><a href="form-input-grid.html"><i class="material-icons">border_inner</i><span class="hide-menu">Form Input Grid</span></a></li>
                            <li><a href="form-hidden-label.html"><i class="material-icons">border_style</i><span class="hide-menu">Form Hidden Label</span></a></li>
                            <li><a href="form-horizontal.html"><i class="material-icons">art_track</i><span class="hide-menu">Form Horizontal</span></a></li>
                            <li><a href="form-bordered.html"><i class="material-icons">filter_frames</i><span class="hide-menu">Form with Border</span></a></li>
                            <li><a href="form-row-separator.html"><i class="material-icons">chrome_reader_mode</i><span class="hide-menu">Form Row Separator</span></a></li>
                            <li><a href="form-row-striped.html"><i class="material-icons">assignment</i><span class="hide-menu">Form Row Striped</span></a></li>
                            <li><a href="form-label-striped.html"><i class="material-icons">border_horizontal</i><span class="hide-menu">Form Label Striped</span></a></li>
                            <li><a href="form-repeater.html"><i class="material-icons">photo_filter</i><span class="hide-menu">Form Repeater</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="form-validation.html" class="collapsible-header"><i class="material-icons">assignment_late</i><span class="hide-menu"> Form Validation </span></a>
                     <div class="collapsible-body">
                        <ul></ul>
                    </div>
                </li>

                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">center_focus_weak</i><span class="hide-menu"> Form Pickers </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="form-picker-colorpicker.html"><i class="material-icons">brightness_4</i><span class="hide-menu">Form Colorpicker</span></a></li>
                            <li><a href="form-picker-datetimepicker.html"><i class="material-icons">perm_contact_calendar</i><span class="hide-menu">Form Datetimepicker</span></a></li>
                            <li><a href="form-picker-material-datepicker.html"><i class="material-icons">brightness_high</i><span class="hide-menu">Form Material Datepicker</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="javascript: void(0);" class="collapsible-header has-arrow"><i class="material-icons">input</i><span class="hide-menu"> Form Editors </span></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="form-editor-ckeditor.html"><i class="material-icons">memory</i><span class="hide-menu">Form Ckeditor</span></a></li>
                            <li><a href="form-editor-tinymce.html"><i class="material-icons">brightness_4</i><span class="hide-menu">Form Tinymce</span></a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="form-wizard.html" class="collapsible-header"><i class="material-icons">developer_board</i><span class="hide-menu"> Form Wizard </span></a>
                     <div class="collapsible-body">
                        <ul></ul>
                    </div>
                </li>  --}}


            </ul>
        </li>
    </ul>
</aside>
