@extends('Admin.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Title and breadcrumb -->
    <!-- ============================================================== -->
    <div class="page-titles">
        <div class="d-flex align-items-center">
            <h5 class="font-medium m-b-0">Badges</h5>
            <div class="custom-breadcrumb ml-auto">
                <a href="#!" class="breadcrumb">Home</a>
                <a href="#!" class="breadcrumb">Badges</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 1</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)
                                @if($Ads->ads_link1)
                                    <input class="form-control input-md" name="price" id="ads_link1" placeholder="Ads link" value="{{$Ads->ads_link1}}" type="text">
                                @else
                                    <input class="form-control input-md" name="price" id="ads_link1" placeholder="Ads link" type="text">
                                @endif
                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_1" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_1)

                        <input class="form-control input-md" name="price" id="id" placeholder="Tags" type="hidden" value="{{$Ads->id}}">

                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_1")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>

            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 2</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link2" placeholder="Ads link" value="{{$Ads->ads_link1}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_2" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_2)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_2")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 3</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link3" placeholder="Ads link" value="{{$Ads->ads_link3}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_3" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_3)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_3")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>

            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 4</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link4" placeholder="Ads link" value="{{$Ads->ads_link4}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_4" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_4)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_4")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 5</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link5" placeholder="Ads link" value="{{$Ads->ads_link5}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_5" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_5)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_5")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>

            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 6</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link6" placeholder="Ads link" value="{{$Ads->ads_link6}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_6" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_6)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_6")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 7</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link7" placeholder="Ads link" value="{{$Ads->ads_link7}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_7" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_7)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_7")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>

            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 8</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link8" placeholder="Ads link" value="{{$Ads->ads_link8}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_8" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_8)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_8")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 9</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link9" placeholder="Ads link" value="{{$Ads->ads_link9}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_9" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_9)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_9")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>

            <div class="col s6">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Ads 10</h5>

                        <div class="form-group mb-4">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link10" placeholder="Ads link" value="{{$Ads->ads_link10}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="ads_10" placeholder="Tags" type="file">

                            {{--  <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>  --}}
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->ads_10)



                        <div class="text-center pad-left-right5">
                            <img src="{{asset("HomeLeftImages/$Ads->ads_10")}}" class="width100 " alt="">
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>
    </div>

    <a href="javascript:void(0);" id="AddHomeRightImage" class="sidenav-trigger right-side-toggle btn-floating btn-large waves-effect waves-light green"><i class="material-icons">save</i></a>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <footer class="center-align m-b-30">All Rights Reserved by Materialart. Designed and Developed by <a href="https://wrappixel.com/">WrapPixel</a>.</footer>
</div>
@endsection
