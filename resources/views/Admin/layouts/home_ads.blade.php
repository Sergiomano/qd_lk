@extends('Admin.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Title and breadcrumb -->
    <!-- ============================================================== -->
    <div class="page-titles">
        <div class="d-flex align-items-center">
            <h5 class="font-medium m-b-0">Badges</h5>
            <div class="custom-breadcrumb ml-auto">
                <a href="#!" class="breadcrumb">Home</a>
                <a href="#!" class="breadcrumb">Badges</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Home page top ads</h5>

                        <div class="form-group mb-3">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="ads_link" placeholder="Ads link" value="{{$Ads->home_top_ads_link}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="file" placeholder="Tags" type="file">

                            <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeTopImage" name="action">Add Image
                            </button>
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->home_top_ads)

                        <input class="form-control input-md" name="price" id="id" placeholder="Tags" type="hidden" value="{{$Ads->id}}">

                        <div class="text-center pad-left15">
                            <img src="{{asset("AdsImage/$Ads->home_top_ads")}}" alt="">
                        </div>
                        @endif
                    @endforeach
                </div>


                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Home page featured top ads</h5>

                        <div class="form-group mb-3">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="featured_ads_link" placeholder="Ads link" value="{{$Ads->home_top_featured_ads_link}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="featured_file" placeholder="Tags" type="file">

                            <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeFeaturedImage" name="action">Add Image
                            </button>
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->home_top_featured_ads)

                        <input class="form-control input-md" name="price" id="id" placeholder="Tags" type="hidden" value="{{$Ads->id}}">

                        <div class="text-center pad-left15">
                            <img src="{{asset("AdsImage/$Ads->home_top_featured_ads")}}" alt="">
                        </div>
                        @endif
                    @endforeach
                </div>


                <div class="card">
                    <div class="card-content">
                        <h5 class="card-title">Home page bottom ads</h5>

                        <div class="form-group mb-3">
                                <label class="control-label">Ads link
                                    </label>
                            @foreach($GetAds as $Ads)

                                <input class="form-control input-md" name="price" id="bottom_ads_link" placeholder="Ads link" value="{{$Ads->home_top_bottom_ads_link}}" type="text">

                            @endforeach

                            <label class="control-label">Upload image
                            </label>
                            <input class="form-control input-md" name="price" id="bottom_file" placeholder="Tags" type="file">

                            <button class="btn cyan waves-effect waves-light right waves-effect" type="submit" id="AddHomeBottomImage" name="action">Add Image
                            </button>
                        </div>
                    </div>

                    <h4 class="pad-left15">Preview</h4>
                    @foreach($GetAds as $Ads)
                        @if($Ads->home_top_bottom_ads)

                        <input class="form-control input-md" name="price" id="id" placeholder="Tags" type="hidden" value="{{$Ads->id}}">

                        <div class="text-center pad-left15">
                            <img src="{{asset("AdsImage/$Ads->home_top_bottom_ads")}}" alt="">
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <footer class="center-align m-b-30">All Rights Reserved by Materialart. Designed and Developed by <a href="https://wrappixel.com/">WrapPixel</a>.</footer>
</div>
@endsection
