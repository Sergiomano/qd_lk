@extends('Admin.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Title and breadcrumb -->
    <!-- ============================================================== -->
    <div class="page-titles">
        <div class="d-flex align-items-center">
            <h5 class="font-medium m-b-0">Cities</h5>
            <div class="custom-breadcrumb ml-auto">
                    <div class="row">
                            <div class="input-field col s12">
                                <button class="btn cyan waves-effect waves-light right waves-effect waves-light btn modal-trigger" href="#AddCities" type="submit" name="action">Add Cities
                                </button>
                            </div>
                        </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        {{-- <h5 class="card-title">Cities</h5> --}}
                        <div class="row">
                            <div class="col s12">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($GetCities as $Cities)
                                        <tr>
                                            <td>{{$Cities->name}}</td>
                                            <td><a href="javascript:void(0);" onclick="EditCities({{$Cities->id}})">Edit</a> | <a href="javascript:void(0);" onclick="DeleteCities({{$Cities->id}})">Delete</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Structure -->
<div id="AddCities" class="modal">
    <div class="modal-content">
        <h4>Add City</h4>
        <div class="card-content">
            <form>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="City Name" id="city" type="text">
                        <label for="name2" class="active">Name</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="button" id="AddCity" name="action">Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="EditCities" class="modal">
        <div class="modal-content">
            <h4>Edit City</h4>
            <div class="card-content">
                <form>
                    <div class="row">
                        <div class="input-field col s12">
                                <input placeholder="City Name" id="Edit_id" type="hidden">

                            <input placeholder="City Name" id="Edit_city" type="text">
                            <label for="name2" class="active">Name</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn cyan waves-effect waves-light right" type="button" id="UpdateCity" name="action">Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <footer class="center-align m-b-30">All Rights Reserved by Materialart. Designed and Developed by <a href="https://wrappixel.com/">WrapPixel</a>.</footer>
</div>
@endsection
