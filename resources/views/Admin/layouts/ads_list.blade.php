@extends('Admin.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Title and breadcrumb -->
    <!-- ============================================================== -->
    <div class="page-titles">
        <div class="d-flex align-items-center">
            <h5 class="font-medium m-b-0">Cities</h5>
            <div class="custom-breadcrumb ml-auto">
                    <div class="row">
                            <div class="input-field col s12">
                                {{--  <button class="btn cyan waves-effect waves-light right waves-effect waves-light btn modal-trigger" href="#AddCities" type="submit" name="action">Add Cities
                                </button>  --}}
                            </div>
                        </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        {{-- <h5 class="card-title">Cities</h5> --}}
                        <div class="row">
                            <div class="col s12">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Category Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($GetAds as $Ads)
                                        <tr>
                                            <td><img src="{{asset("Pay_Featured_Image/$Ads->filename")}}" alt="" style="width: 150px;"></td>
                                            <td>{{$Ads->title}}</td>
                                            <td>{{$Ads->name}}</td>
                                            <td><a href="/Admin/EditPosts/{{$Ads->id}}">Edit</a> | <a href="javascript:void(0);" onclick="DeleteCities({{$Ads->id}})">Delete</a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
