@extends('Admin.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Title and breadcrumb -->
    <!-- ============================================================== -->
    <div class="page-titles">
        <div class="d-flex align-items-center">
            <h5 class="font-medium m-b-0">Add Ads</h5>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid scss in scafholding.scss -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col s12 l12">
                <div class="card">
                    <div class="card-content">
                        {{--  <h5 class="card-title activator">Form with placeholder<i class="material-icons right tooltipped" data-position="left" data-delay="50" data-tooltip="Get Code">more_vert</i></h5>  --}}
                        <form id="PostDatas">
                            <div class="row">
                                <div class="input-field col s12">
                                        <input class="form-control input-md" name="Title" id="AdsId" type="hidden" value="{{$GetPosts['id']}}" placeholder="Title" type="text">
                                    <input placeholder="Ads name" id="title" value="{{$GetPosts['title']}}" type="text">
                                    <label for="name2">Ads Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m6 l6">
                                    <div class="input-field">
                                        <select id="categories" onchange="CheckSubCategories()">
                                            <option value="" disabled selected>Choose your option</option>
                                            @foreach($GetCategories as $Categories)
                                            <option value="{{$Categories->translation_of}}" {{$Categories->translation_of == $GetPosts->category_id  ? 'selected' : ''}}>{{$Categories->name}}
                                            </option>
                                            @endforeach
                                        </select>
                                        <label>Categories</label>
                                    </div>
                                </div>

                                <div class="col s12 m6 l6">
                                        <div class="input-field SubCategoriesShow">
                                            <select id="SubCategories">
                                                <option value="" disabled selected>Choose your option</option>
                                                @foreach($GetSubcategories as $Categories)
                        <option value="{{$Categories->parent_id}}" {{$Categories->parent_id == $GetPosts->sub_category_id  ? 'selected' : ''}}>{{$Categories->name}}
                        </option>
                        @endforeach
                                            </select>
                                            <label>Sub Categories</label>
                                        </div>
                                    </div>


                                <div class="input-field col s6">
                                    <input placeholder="Price" id="price" type="text" value="{{$GetPosts['price']}}">
                                    <label for="password2">Price</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12 m6 l6">
                                    <div class="input-field">
                                        <select id="city">
                                            @foreach($GetCities as $Cities)
                                            <option value="{{$Cities->id}}" {{$Cities->id == $GetPosts->city_id  ? 'selected' : ''}}>{{$Cities->name}}</option>
                                          @endforeach
                                        </select>
                                        <label>City</label>
                                    </div>
                                </div>

                                <div class="input-field col s6">
                                    <input placeholder="tags" id="tags" type="text" value="{{$GetPosts['tags']}}">
                                    <label for="password2">Tags</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea placeholder="" id="post_description" class="materialize-textarea">{{$GetPosts['description']}}</textarea>
                                    <label for="message2">Description</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                        {{-- <label for="name2">Upload Featured image</label> --}}
                                    <input placeholder="Ads name" id="file" type="file">

                                </div>



                                <div class="width50 pull-left featured-img-pad-left">
                                        <img class="featured-img-width" src="{{URL::asset('Pay_Featured_image')}}/{{$GetFeaturedImage->filename}}" alt="" style="width: 150px;">
                                      </div>
                            </div>
                        </form>

                        <div class="row ShoworHideDropImages">
                                <div class="input-field col s12">
                                        <label class="control-label">Upload Multiple Images
                                            </label>
                                    <form method="post" action="{{url('image/upload/Adminstore')}}" enctype="multipart/form-data" class="dropzone" id="dropzone" >
                                            @csrf
                                        </form>

                                        <br>

                                        <div class="row" id="PostImages">
                                            @foreach($GetPostImages as $PostImages)
                                            <div class="col s3">

                                                    <img class="post_images" src="{{URL::asset('public/Post_images/')}}/{{$PostImages->post_images}}" alt="">
                                                    <a href="javascript:void(0);" onclick="DeletePostimages({{$PostImages->id}})">Delete</a>
                                            </div>

                                          @endforeach
                                        </div>

                                  <button class="btn btn-common" id="UpdateAds" type="button" id="">Post Ad
                                    </button>
                                </div>
                            </div>


                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">Code<i class="material-icons right">close</i></span>
                        <pre class="pre-scroll">                                    <code class="language-markup">
                                &lt;form&gt;
                                    &lt;div class=&quot;row&quot;&gt;
                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                            &lt;input placeholder=&quot;John Doe&quot; id=&quot;name2&quot; type=&quot;text&quot;&gt;
                                            &lt;label for=&quot;name2&quot;&gt;Name&lt;/label&gt;
                                        &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;div class=&quot;row&quot;&gt;
                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                            &lt;input placeholder=&quot;<a href="https://www.wrappixel.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="751f1a1d1b35111a18141c1b1b1418105b161a18">[email&#160;protected]</a>&quot; id=&quot;email2&quot; type=&quot;email&quot;&gt;
                                            &lt;label for=&quot;email2&quot;&gt;Email&lt;/label&gt;
                                        &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;div class=&quot;row&quot;&gt;
                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                            &lt;input placeholder=&quot;YourPassword&quot; id=&quot;password2&quot; type=&quot;password&quot;&gt;
                                            &lt;label for=&quot;password2&quot;&gt;Password&lt;/label&gt;
                                        &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;div class=&quot;row&quot;&gt;
                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                            &lt;textarea placeholder=&quot;Oh WoW! Let me check this one too.&quot; id=&quot;message2&quot; class=&quot;materialize-textarea&quot;&gt;&lt;/textarea&gt;
                                            &lt;label for=&quot;message2&quot;&gt;Message&lt;/label&gt;
                                        &lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;div class=&quot;row&quot;&gt;
                                        &lt;div class=&quot;input-field col s12&quot;&gt;
                                            &lt;button class=&quot;btn cyan waves-effect waves-light right&quot; type=&quot;submit&quot; name=&quot;action&quot;&gt;Submit
                                            &lt;/button&gt;
                                        &lt;/div&gt;
                                    &lt;/div&gt;
                                &lt;/form&gt;
                            </code>
                        </pre>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
