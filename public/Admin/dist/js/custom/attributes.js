$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


// Dropzone.autoDiscover = false;

// var myDropzone = new Dropzone("#dropzone", {
//    autoProcessQueue: false,
//    parallelUploads: 10 // Number of files process at a time (default 2)
// });



$(document).on("click", "#AddCity", function(){
    var city = $("#city").val();



    var CityData = {
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/AddCities",
        data: CityData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                location.reload();
                return false;
            }
        }
    });
});



$(document).on("click", "#AddHomeTopImage", function(){
    var id = $("#id").val();
    var ads_link = $("#ads_link").val();

    var AdsData = new FormData();

    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    AdsData.append('id', id);
    AdsData.append('ads_link', ads_link);
    AdsData.append('file', $('#file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/AddHomeTopAds",
    data: AdsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            location.reload();
            return false;
        }
    }
});
});


function EditCities(Id){
    $.ajax({
        type: "POST",
        url: "/GetCities/"+Id,
        data: {Id: Id},
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                $("#Edit_id").val(data.id);
                $("#Edit_city").val(data.name);

                $("#EditCities").modal('open');
            }
        }
    });
  }



  $(document).on("click", "#UpdateCity", function(){
    var id = $("#Edit_id").val();
    var city = $("#Edit_city").val();



    var CityData = {
        id: id,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/UpdateCities",
        data: CityData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                location.reload();
                return false;
            }
        }
    });
});




function DeleteCities(id){
    $.ajax({
            type: "POST",
            url: "/DeleteCities/"+id,
            data: {id: id},
            dataType: "JSON",
            success: function (data) {
                if(data.error){
                    swal("Error", data.message, "error");
                    return false;
                }else{
                    swal("Success", data.message);
                    location.reload();
                    return false;
                }
            }
        });
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     closeOnConfirm: false
    // }, function () {
    //     $.ajax({
    //         type: "POST",
    //         url: "/DeleteCities/"+id,
    //         data: {id: id},
    //         dataType: "JSON",
    //         success: function (data) {
    //             if(data.error){
    //                 swal("Error", data.message, "error");
    //                 return false;
    //             }else{
    //                 swal("Success", data.message);
    //                 location.reload();
    //                 return false;
    //             }
    //         }
    //     });
    // });
}














// ADS
$(document).on("click", "#AddHomeFeaturedImage", function(){
    var id = $("#id").val();
    var featured_ads_link = $("#featured_ads_link").val();

    var AdsData = new FormData();

    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    AdsData.append('id', id);
    AdsData.append('featured_ads_link', featured_ads_link);
    AdsData.append('file', $('#featured_file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/AddHomeFeaturedTopAds",
    data: AdsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            location.reload();
            return false;
        }
    }
});
});



$(document).on("click", "#AddHomeBottomImage", function(){
    var id = $("#id").val();
    var bottom_ads_link = $("#bottom_ads_link").val();

    var AdsData = new FormData();

    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    AdsData.append('id', id);
    AdsData.append('bottom_ads_link', bottom_ads_link);
    AdsData.append('file', $('#bottom_file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/AddHomeBottomTopAds",
    data: AdsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            location.reload();
            return false;
        }
    }
});
});






$(document).on("click", "#AddHomeLeftImage", function(){
    var id = $("#id").val();
    var ads_link1 = $("#ads_link1").val();
    var ads_link2 = $("#ads_link2").val();
    var ads_link3 = $("#ads_link3").val();
    var ads_link4 = $("#ads_link4").val();
    var ads_link5 = $("#ads_link5").val();
    var ads_link6 = $("#ads_link6").val();
    var ads_link7 = $("#ads_link7").val();
    var ads_link8 = $("#ads_link8").val();
    var ads_link9 = $("#ads_link9").val();
    var ads_link10 = $("#ads_link10").val();

    var AdsData = new FormData();

    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    AdsData.append('id', id);
    AdsData.append('ads_link1', ads_link1);
    AdsData.append('ads_link2', ads_link2);
    AdsData.append('ads_link3', ads_link3);
    AdsData.append('ads_link4', ads_link4);
    AdsData.append('ads_link5', ads_link5);
    AdsData.append('ads_link6', ads_link6);
    AdsData.append('ads_link7', ads_link7);
    AdsData.append('ads_link8', ads_link8);
    AdsData.append('ads_link9', ads_link9);
    AdsData.append('ads_link10', ads_link10);
    AdsData.append('file1', $('#ads_1')[0].files[0]);
    AdsData.append('file2', $('#ads_2')[0].files[0]);
    AdsData.append('file3', $('#ads_3')[0].files[0]);
    AdsData.append('file4', $('#ads_4')[0].files[0]);
    AdsData.append('file5', $('#ads_5')[0].files[0]);
    AdsData.append('file6', $('#ads_6')[0].files[0]);
    AdsData.append('file7', $('#ads_7')[0].files[0]);
    AdsData.append('file8', $('#ads_8')[0].files[0]);
    AdsData.append('file9', $('#ads_9')[0].files[0]);
    AdsData.append('file10', $('#ads_10')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/AddHomeLeftAds",
    data: AdsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            location.reload();
            return false;
        }
    }
});
});








$(document).on("click", "#PostAds", function(){
    var title = $("#title").val();
    var categories = $("#categories").val();
    var subcategories = $("#SubCategories").val();
    var description = CKEDITOR.instances['post_description'].getData();
    var price = $("#price").val();
    var city = $("#city").val();
    var tags = $("#tags").val();



// var PostsData = {
//     title: title,
//     categories: categories,
//     description: description,
//     price: price,
//     tags: tags,
//     city: city
// }

var PostsData = new FormData();

    PostsData.append('title', title);
    PostsData.append('categories', categories);
    PostsData.append('subcategories', subcategories);
    PostsData.append('description', description);
    PostsData.append('price', price);
    PostsData.append('tags', tags);
    PostsData.append('city', city);
    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    PostsData.append('file', $('#file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/Admin/AddPosts",
    data: PostsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            $("#PostDatas").hide();
            $(".ShoworHideDropImages").show();
            return false;
        }
    }
});
});








$(document).on("click", "#AddHomeRightImage", function(){
    var id = $("#id").val();
    var ads_link1 = $("#ads_link1").val();
    var ads_link2 = $("#ads_link2").val();
    var ads_link3 = $("#ads_link3").val();
    var ads_link4 = $("#ads_link4").val();
    var ads_link5 = $("#ads_link5").val();
    var ads_link6 = $("#ads_link6").val();
    var ads_link7 = $("#ads_link7").val();
    var ads_link8 = $("#ads_link8").val();
    var ads_link9 = $("#ads_link9").val();
    var ads_link10 = $("#ads_link10").val();

    var AdsData = new FormData();

    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    AdsData.append('id', id);
    AdsData.append('ads_link1', ads_link1);
    AdsData.append('ads_link2', ads_link2);
    AdsData.append('ads_link3', ads_link3);
    AdsData.append('ads_link4', ads_link4);
    AdsData.append('ads_link5', ads_link5);
    AdsData.append('ads_link6', ads_link6);
    AdsData.append('ads_link7', ads_link7);
    AdsData.append('ads_link8', ads_link8);
    AdsData.append('ads_link9', ads_link9);
    AdsData.append('ads_link10', ads_link10);
    AdsData.append('file1', $('#ads_1')[0].files[0]);
    AdsData.append('file2', $('#ads_2')[0].files[0]);
    AdsData.append('file3', $('#ads_3')[0].files[0]);
    AdsData.append('file4', $('#ads_4')[0].files[0]);
    AdsData.append('file5', $('#ads_5')[0].files[0]);
    AdsData.append('file6', $('#ads_6')[0].files[0]);
    AdsData.append('file7', $('#ads_7')[0].files[0]);
    AdsData.append('file8', $('#ads_8')[0].files[0]);
    AdsData.append('file9', $('#ads_9')[0].files[0]);
    AdsData.append('file10', $('#ads_10')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/AddHomeRightAds",
    data: AdsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            location.reload();
            return false;
        }
    }
});
});
// END


function CheckSubCategories(){
    var CategoryId = $("#categories").val();

    $.ajax({
        type: "POST",
        url: "/GetSubcategories/"+CategoryId,
        data: {CategoryId: CategoryId},
        dataType: "html",
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SubCategoriesShow").show();
                $("#SubCategories").val("");
                $("#SubCategories").html(data);
            }
        }
    });
}


$(document).on("click", "#PostAdsSuccess", function(){
    alert("Post successfully");
    location.reload();
    return false;
});



$(document).on("click", "#UpdateAds", function(){

    var AdsId = $("#AdsId").val();
    var title = $("#title").val();
    var categories = $("#categories").val();
    var subcategories = $("#SubCategories").val();
    var description = CKEDITOR.instances['post_description'].getData();
    var price = $("#price").val();
    var city = $("#city").val();
    var tags = $("#tags").val();


// var PostsData = {
//     title: title,
//     categories: categories,
//     description: description,
//     price: price,
//     tags: tags,
//     city: city
// }

var PostsData = new FormData();

    PostsData.append('AdsId', AdsId);
    PostsData.append('title', title);
    PostsData.append('categories', categories);
    PostsData.append('subcategories', subcategories);
    PostsData.append('description', description);
    PostsData.append('price', price);
    PostsData.append('tags', tags);
    PostsData.append('city', city);
    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    PostsData.append('file', $('#file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/UpdatePosts",
    data: PostsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            danger_toast_msg(data.message);
            return false;
        }else{
            success_toast_msg(data.message);
            alert(data.message);

            return false;
        }
    }
});
});



function DeletePostimages(id){
    // alert("check");
    // return false

    $.ajax({
        type: "POST",
        url: "/DeletePictures/"+id,
        // data: "",
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                // $("#PostImages").load(location.href + " #PostImages");
                location.reload();
                return false;
            }
        }
    });
}
