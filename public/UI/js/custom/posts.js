$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Dropzone.autoDiscover = false;

// var myDropzone = new Dropzone("#dropzone", {
//    autoProcessQueue: false,
//    parallelUploads: 10 // Number of files process at a time (default 2)
// });


$(document).on("click", "#PostAds", function(){
    var title = $("#title").val();
    var categories = $("#categories").val();
    var subcategories = $("#SubCategories").val();
    var description = CKEDITOR.instances['post_description'].getData();
    var price = $("#price").val();
    var city = $("#city").val();
    var tags = $("#tags").val();



// var PostsData = {
//     title: title,
//     categories: categories,
//     description: description,
//     price: price,
//     tags: tags,
//     city: city
// }

var PostsData = new FormData();

    PostsData.append('title', title);
    PostsData.append('categories', categories);
    PostsData.append('subcategories', subcategories);
    PostsData.append('description', description);
    PostsData.append('price', price);
    PostsData.append('tags', tags);
    PostsData.append('city', city);
    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    PostsData.append('file', $('#file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/AddPosts",
    data: PostsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    beforeSend: function() {
        // setting a timeout
        $("#PostAds").attr("disabled", 'disabled');
    },
    success: function (data) {
        if(data.error){
            alert(data.message);
            return false;
        }else{
            alert(data.message);
            $("#PostDatas").hide();
            $(".ShoworHideDropImages").show();
            return false;
        }
    },
    complete: function() {
        $("#PostAds").removeAttr("disabled", 'disabled');
    }
});
});


$(document).on("click", "#UpdateAds", function(){
    var AdsId = $("#AdsId").val();
    var title = $("#title").val();
    var categories = $("#categories").val();
    var subcategories = $("#SubCategories").val();
    var description = CKEDITOR.instances['post_description'].getData();
    var price = $("#price").val();
    var city = $("#city").val();
    var tags = $("#tags").val();


// var PostsData = {
//     title: title,
//     categories: categories,
//     description: description,
//     price: price,
//     tags: tags,
//     city: city
// }

var PostsData = new FormData();

    PostsData.append('AdsId', AdsId);
    PostsData.append('title', title);
    PostsData.append('categories', categories);
    PostsData.append('subcategories', subcategories);
    PostsData.append('description', description);
    PostsData.append('price', price);
    PostsData.append('tags', tags);
    PostsData.append('city', city);
    // CandidateProfileData.append('_token', $("input[name=_token]").val());
    PostsData.append('file', $('#file')[0].files[0]);

$.ajax({
    type: "POST",
    url: "/UpdatePosts",
    data: PostsData,
    dataType: "JSON",
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.error){
            danger_toast_msg(data.message);
            return false;
        }else{
            success_toast_msg(data.message);
            return false;
        }
    }
});
});



function DeletePostimages(id){
    // alert("check");
    // return false

    $.ajax({
        type: "POST",
        url: "/DeletePictures/"+id,
        // data: "",
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                alert(data.message);
                return false;
            }else{
                alert(data.message);
                // $("#PostImages").load(location.href + " #PostImages");
                location.reload();
                return false;
            }
        }
    });
}



function CheckSubCategories(){
    var CategoryId = $("#categories").val();

    $.ajax({
        type: "POST",
        url: "/GetSubcategories/"+CategoryId,
        data: {CategoryId: CategoryId},
        dataType: "html",
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{
                $("#SubCategoriesShow").show();
                $("#SubCategories").val("");
                $("#SubCategories").html(data);
            }
        }
    });
}



// Favourites
$(function() {
    $(".heart").on("click", function() {
        var post_id = $(this).data("id");
        if($(this).toggleClass("is-active")){

                $.ajax({
                    type: "POST",
                    url: "/AddToFavourites",
                    data: {post_id: post_id},
                    dataType: "JSON",
                    success: function (data) {
                        if(data.error){
                            danger_toast_msg(data.message);
                            return false;
                        }else{
                            success_toast_msg(data.message);
                            return false;
                        }
                    }
                });
        }
    });
  });





  $(document).ready(function() {
    src = "/searchajax";
     $("#ProductName").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);

                }
            });
        },
        minLength: 1,

    });
});




function ChangeSortBy(){
    var sortby = $("#sortby").val();

    $.ajax({
        type: "POST",
        url: "/GetSortBy",
        data: {sortby: sortby},
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#AdsResults").hide();
            // return false;
        },
        success: function (data) {
            if(data.error){
                console.log(data);
                return false;
            }else{

                $("#AdsResults").html("");
                $("#AdsResults").append(data);

                return false;
            }
        },
        complete:function(data){
        // Hide image container
            $("#AjaxLoader").hide();
            $("#AdsResults").show();
        }
    });
}


$(document).on("click", "#PostAdsSuccess", function(){
    alert("Post successfully");
    location.reload();
    return false;
});


function success_toast_msg(data){
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: '#37BFA7',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}

function danger_toast_msg(data){
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: 'red',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}




