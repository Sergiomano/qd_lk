$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});




$(document).on("click", "#UpdateProfile", function(){
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();

    var name = first_name +" "+ last_name;
    var phone = $("#phone").val();
    var email = $("#email").val();
    var address = $("#address").val();
    var state = $("#state").val();
    var city = $("#city").val();

    var UserProfileData = {
        name: name,
        phone: phone,
        email: email,
        address: address,
        state: state,
        city: city
    }

    $.ajax({
        type: "POST",
        url: "/ProfileAddOrUpdate",
        data: UserProfileData,
        dataType: "JSON",
        success: function (data) {
            if(data.error){
                danger_msg(data.message);
                return false;
            }else{
                success_msg(data.message);
                return false;
            }
        }
    });
});