$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).on("click", "#Register", function(){
    $("#LoginForm").hide();
    $("#RegisterForm").show();
});

$(document).on("click", "#BacktoLogin", function(){
    $("#LoginForm").show();
    $("#RegisterForm").hide();
});
// Login
$(document).on("click", "#SignIn", function(){
    // alert("check");
    // return false;
    var login_name = $("#login_name").val();
    var login_password = $("#user_password").val();

    if(login_name == ""){
        danger_toast_msg("Username should not empty");
        $("#login_name").focus();
        return false;
    }

    if(login_password == ""){
        danger_toast_msg("Password should not empty");
        $("#user_password").focus();
        return false;
    }

    var SigninData = {
        login_name: login_name,
        login_password: login_password
    }

    // console.log(login_password);
    // return false;
    $.ajax({
        type: "POST",
        url: "/CheckLogin",
        data: SigninData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxLoader").show();
            $("#SignIn").attr("disabled", 'disabled');
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                location.href = "/Users/Dashboard";
                return false;
            }
        },
        complete:function(data){
            // Hide image container
                $("#AjaxLoader").hide();
                $("#SignIn").removeAttr("disabled", 'disabled');
            }
    });
});


// SignUp

$(document).on("click", "#Signup", function(){
    var name = $("#reg_name").val();
    var email = $("#reg_email").val();
    var password = $("#reg_password").val();
    var reg_confirm_password = $("#reg_confirm_password").val();

    if(name == ""){
        danger_toast_msg("Username should not empty");
        $("#reg_name").focus();
        return false;
    }

    if(email == ""){
        danger_toast_msg("Email should not empty");
        $("#reg_email").focus();
        return false;
    }

    if(password == ""){
        danger_toast_msg("Password should not empty");
        $("#reg_password").focus();
        return false;
    }

    if(reg_confirm_password == ""){
        danger_toast_msg("Confirm Password should not empty");
        $("#reg_confirm_password").focus();
        return false;
    }

    if(password != reg_confirm_password){
        danger_toast_msg("Password does not match");
        $("#reg_confirm_password").focus();
        return false;
    }

    var SignupData = {
        name: name,
        email: email,
        password: password
    }

    $.ajax({
        type: "POST",
        url: "/AddUsers",
        data: SignupData,
        dataType: "JSON",
        beforeSend: function(){
            // Show image container
            $("#AjaxRegisterLoader").show();
            $("#Signup").attr("disabled", 'disabled');
            // return false;
        },
        success: function (data) {
            if(data.error){
                danger_toast_msg(data.message);
                return false;
            }else{
                success_toast_msg(data.message);
                return false;
            }
        },
        complete:function(data){
            // Hide image container
                $("#AjaxRegisterLoader").hide();
                $("#Signup").removeAttr("disabled", 'disabled');
            }
    });
});



function success_msg(data){
    $.notify({
        title: data,
        message: ''
    },{
        type: 'success'
    });
}

function danger_msg(data){
    $.notify({
        title: data,
        message: ''
    },{
        type: 'danger'
    });
}

function success_toast_msg(data){
    // $.notify({
    //     title: '<strong>'+ data +'</strong>',
    //     message: ''
    // },{
    //     type: 'success'
    // });
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: '#37BFA7',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}

function danger_toast_msg(data){
    // $.notify({
    //     title: '<strong>'+ data +'</strong>',
    //     message: ''
    // },{
    //     type: 'danger'
    // });
    var toast = new iqwerty.toast.Toast();
toast
.setText(data)
.stylize({
  background: 'red',
  color: 'white',
  'box-shadow': '0 0 50px rgba(0, 0, 0, .7)'
})
.show();
}
